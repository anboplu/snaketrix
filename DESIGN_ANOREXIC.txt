:::DESIGN:::
::terminology::
-trixy
	-the anorexic snake
-object
	-occupied square on the screen
-catch
	-snake encircles around object
-hit
	-snake's head runs into an object
-snake
	-snake
-wall
	-motionless object
-mouse
	-mouse
::core::
-no portals!
:global properties:
-units
	-speed is measured in squares/frame (speed must be in [-1,1])
:snake:
-visibility
	-only visible at points of self intersection
	-possibly visible at head if too hard
	-flashing
		-once at beginning of level
		-whenever it hits the wall
		-or maybe on a cooldown
	-snake tiles adjacent to mouse tiles are visible (8 directional adjacency)
-movement
	-speed
		-constant speed
		-may change with level progression
	-interaction
		-walls	
			-flashes (see above)
			-turns randomly left or right
			-may die
		-mice
			-flashes
			-may die
			-may grow longer
:mouse:
-movement
	-speed
		-constant speed
		-may change with level progression
	-pattern
		-each move is in a random direction
		-can not move through snakes and walls or other mice
		-possibly will include more complicated patterns
:walls:
-types
	-boundary wall
		-looks like a wall
	-boulder wall
		-looks like a boulder
:mouse holes:
-variables
	-spawn rate 
		-changes with level progression (increases)
	-max number mice
		-changes with level progression
		-could be infinity
-behaviour
	-spawns mice every getSpawnRate(level) frames
-has orientation
-also act as wall
:objectives:
-level progression
	-catch certain number of mice
	-condition(length)
:stage:
-all levels procedurally generated
-boundaries
	-may have asteroid style boundaries
	-may have wall boundaries
-walls
	-may be randomly generated on the screen
-mouse holes
	-exist on boundaries
	-may eventually exist in middle of screen
-rules for procedural generation
	-number of walls and mouse holes change
	-placement of walls and mouse holse random
-timer
	-backwards timer when hits 0 you lose
:score:
-time score
-plus points for each caught mouse
-minus points fer each object hit
-minus points for each intersection
:menu:
-typical anboplu and gen gui logos
-start
	-starts game at level 0
-level select
	-one up/down selector for level
	-starts at 0
-controls (optional)
-local high score (just one score)
-exit
	-quits game
:story:
-cutscenes in between level that tell the story of the anorexic snake
	-press button to progress to next frame/level
	
::graphics::
:specs:
-squares
	-20x20
-format
	-bmp 
	-24 bit colors
	-black is invisible
-no animations
-walls
	-top view
	-one wall and on eboulder graphic
	-directionless
-mouse hole
	-side view
	-has direction
	-<insert ascii drawing of mouse hole here>
-mouse
	-side view
	-no direction
	-may have one frame death animation
-snake
	-head
		-side or top?
		-may have direction (if top view)
	-intersection	
		-no direction
	-may have death animation
-cutscenes
	-in between level
	-game over cutscenes
:menu:
-make anboplu logo
	-with wavy effect
-font set
	-decide size
	-potentially full ascii
	-higher resolution than dragon game font
-finish this section peter
	
:style:

::audio::
:music:
-title screen music?
-exists
:sounds:
-catch
-hit mouse
-hit wall
-intersect self
-mouse close to snake
-mouse out of hole
-new level
-win level
-lose level

::story::

::tech::
- NO SMART POINTERS
	- okay, maybe just a little bit
	- no SSP pointers nonsense, only SP pointers
- Controller (keyboard input)
	- Logos
	- Menu
	- Story
	- Game
	- Game Over
- Font
- Animation (w. sound)
	- Fade Out
- Score
	- High Score

- Drawing gui
- Discrete graphics Class
- Additional gui for camera, zoom, etc
- make framerate variable
- new graphics&sound preloader

Future Notes
	- single svn repo for include files?
	- try generalize/reuse (start thinking about it)
	- generealize preloader to load sounds by putting freaky functional programming stuff in there

	- MyPreloader needs reworking, same with sequences
	- new file data read manager