#ifndef TRACKBALLMAN_HEADER_04_09_11
#define TRACKBALLMAN_HEADER_04_09_11
#include <list>
#include "GuiGen.h"
#include "General.h"

enum TrackBallManDirEnum
{
	TBM_LEFT,
	TBM_RIGHT,
	TBM_UP,
	TBM_DOWN,
	TBM_NEUTRAL
};

class TrackBallMan
{
	std::list<Gui::Point> mPos;
	TrackBallManDirEnum getDirection(Gui::Point aPt)
	{
		if(aPt == Gui::Point())
			return TBM_NEUTRAL;
		if(Gabs(aPt.x) >= Gabs(aPt.y))
		{
			if(aPt.x > 0)
				return TBM_RIGHT;
			else
				return TBM_LEFT;
		}
		else
		{
			if(aPt.y > 0)
				return TBM_UP;
			else
				return TBM_DOWN;
		}
		return TBM_NEUTRAL;
	}
public:
	
	void mouseMoved(Gui::Point aRel)
	{
		mPos.push_back(aRel);
		if(mPos.size() > 30)
			mPos.pop_front();
	}
	TrackBallManDirEnum getMoveDirection()
	{
		std::list<Gui::Point>::iterataor it = mPos.end();
		Gui::Point last = *(--it);
		Gui::Point lastm1 = *(--it);
		Gui::Point lastm2 = *(--it);
		Gui::Point c1 = last - lastm1;
		Gui::Point c2 = lastm1 - lastm2;
		if(Gui::fPoint(c1).Length() < 2)
			return TBM_NEUTRAL;	
		if(getDirection(c1) != getDirection(c2))
			return getDirection(c1);
		return TBM_NEUTRAL;
	}


	
};


#endif //TRACKBALLMAN_HEADER_04_09_11