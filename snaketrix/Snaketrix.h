#ifndef SNAKETRIX_HEADER_08_12_10_09_39
#define SNAKETRIX_HEADER_08_12_10_09_39

#include "SmartPointer.h"
#include "Preloader.h"
#include "event.h"
#include "Global.h"
#include <vector>
#include <iostream>



//#define CUTE_AND_COMFY
//#define ENABLE_CHEATS
#define ENABLE_MULTIPLAYER
#define OLD_MOUSE_MOVEMENT


//#define GAME_DEVICE_TYPE GD_MOBILE
#define GAME_DEVICE_TYPE GD_PC
#ifdef CUTE_AND_COMFY
#undef ENABLE_MULTIPLAYER
#endif


//#include "GlobalSoundPlayer.h"

//sound channels
#define BG_MUSIC_CHANNEL 0

using namespace Gui;

typedef CameraControl<Index> MyCamera;

typedef SoundInterface<Index> SoundManager;

extern unsigned nFrameRate;
extern bool bIgnoreSpaceReturn;

enum GAME_DEVICE {GD_PC, GD_ARCADE, GD_MOBILE};



class GameController: virtual public SP_Info
{
public:
    virtual void OnKey(GuiKeyType nCode, bool bUp){};
    virtual void OnClick(Point p){};
    virtual void Update(){};
};


class Animation: virtual public SP_Info
{
public:
    virtual void Update(MyCamera& cc, SP<SoundManager> pSnd)=0;
};

class AnbopluLogoController : public GameController
{
	MyCamera cc;
    SP<SoundManager> pSnd;
	MyPreloader& pr;
	std::vector<Point> p;
	Point center;
	Index img;
	int totalWidth;
	int counter;
	int scaleFactor;
	SP<Event> pEvTerminate;

	bool bEsc;
public:
	//TODO get actual screen center
	AnbopluLogoController(MyCamera cc_,SP<SoundManager> pSnd_,SP<Event> pEvTerminate_, Index img_, MyPreloader& pr_, bool bEsc_)
        :cc(cc_), pSnd(pSnd_),pEvTerminate(pEvTerminate_),center(cc.fromF(fPoint(.5, .5))),scaleFactor(3), img(img_),
		pr(pr_), bEsc(bEsc_)
	{
		p.push_back(Point(22,22));	//A
		p.push_back(Point(12,22));	//N
		p.push_back(Point(11,22));	//B
		p.push_back(Point(11,22));	//O
		p.push_back(Point(11,22));	//P
		p.push_back(Point(10,22));	//L
		p.push_back(Point(11,22));	//U
		for(unsigned i = 0; i < p.size(); i++)
		{
			p[i] = Point(p[i].x*scaleFactor,p[i].y*scaleFactor);
		}

		totalWidth = p.size() + 1;
		for(unsigned i = 0; i < p.size(); i++)
			totalWidth += p[i].x;
		img = cc.pGr->ScaleImage(img,scaleFactor);
		cc.pGr->GetImage(img)->ColorTransparent(Color(0,0,0));
		counter = 0;

	}
private:
	void drawImage(int r, int c)
	{
		//topleft of image we want to draw
		int drawX = r*scaleFactor+scaleFactor;
		int drawY = c*scaleFactor+scaleFactor;
		for(int i = 0; i < r; i++)
		{
			drawX += p[i].x;
		}
		drawY += p[r].y*c;
		Rectangle DrawR(Point(drawX,drawY),Size((p[r]+Point(2,2)).x,(p[r]+Point(2,2)).y));
		//std::cout << DrawR.p << " " << DrawR.sz.x << " " << DrawR.sz.y << std::endl;
		cc.pGr->DrawImage(Point(center.x - totalWidth/2 + drawX,center.y - p[r].y/2),img,DrawR,true);
	}
	void Terminate(){Trigger(pEvTerminate);}
	bool drawLogo(int _iter)
	{		
		int iterations = 8;
		int pause = 10;
		if(_iter < int(iterations*2 - 1 + p.size() + 2))
		{
			//do regular blink
			for(unsigned i = 0; i < p.size(); i++)
			{
				//calculate distance from peak of blink
				int d = -iterations+_iter;
				int c = Gabs(d-int(i));
				drawImage(i,c);
			}
			return false;
		}
		else if(_iter >  int(iterations*2 - 1 + p.size() + 15))
		{
			Terminate();
		}
		/*
		else if(_iter <= iterations*2 - 1 + p.size()*2)
		{
			for(int i = 0; i < p.size(); i++)
			{
				//final blink
				int t = _iter - iterations*2 - 1 + p.size();
				drawImage(i,abs(i-iterations+t));
			}
			return false;
		}	
		else
		{
			//always on or fade off
			Terminate();
			return true;
		}
		*/
        return false;
	}
public:
	/*virtual*/ void Update();/*
	{
		drawLogo(counter);
		counter++;
		if (!menuMusicPlaying)
		{

			GlobalSoundPlayer::stopSound(BG_MUSIC_CHANNEL);
			GlobalSoundPlayer::playSound("menu_music", BG_MUSIC_CHANNEL);
			//pSnd->PlaySound(pr.GetSnd("menu_music"), BG_MUSIC_CHANNEL, true);
			menuMusicPlaying = true;
		}
	}*/

	/*virtual*/ void OnKey(GuiKeyType nCode, bool bUp)
	{
		if(bUp)
			return;

		if(!bEsc || nCode == GUI_ESCAPE)
			Terminate();
	}


	/*virtual*/ void OnClick(Point p)
	{
		Terminate();
	}
};

class AnimationController: public GameController
{
    MyCamera cc;
    SP<SoundManager> pSnd;
    Rectangle rBound;
    Color cBkg;
    
    std::list< SP<Animation> > lsAnim;

	bool bStacked;

public:
	int nTheme;

	AnimationController(MyCamera cc_, SP<SoundManager> pSnd_, Rectangle rBound_, Color cBkg_ = Color(), bool bStacked_ = false)
        :cc(cc_), pSnd(pSnd_), rBound(rBound_), cBkg(cBkg_), bStacked(bStacked_), nTheme(-1){}

    /*virtual*/ void Update();

    void NewAnimation(SP<Animation> pAn){lsAnim.push_back(pAn);}
};

class EventAnimationController: public AnimationController
{
    SP<Event> pEvTerminate;
    bool bEsc;
public:

	std::map<GuiKeyType, SP<Event> > mpKeyEvents;

    EventAnimationController(MyCamera cc_, SP<SoundManager> pSnd_, Rectangle rBound_,
        Color cBkg_, SP<Event> pEvTerminate_, bool bEsc_, bool bStacked = false)
        :AnimationController(cc_, pSnd_, rBound_, cBkg_, bStacked), pEvTerminate(pEvTerminate_), bEsc(bEsc_){}

    void Terminate(){Trigger(pEvTerminate);}
    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void OnClick(Point p);
};

class StaticImage: public Animation
{
    Point p;
    Index img;

	bool bCenter;
public:
    StaticImage(Point p_, Index img_, bool bCenter_ = false):p(p_), img(img_), bCenter(bCenter_){}

    /*virtual*/ void Update(MyCamera& cc, SP<SoundManager> pSnd);

};

class AnimationOnce: public Animation
{
    SP<Event> pEvTerminate;

protected:
    bool bDead;

    Point p;

    Timer t;
    Timer t2;
    ImageSequence sqImg;

    bool bCenter;

public:
    AnimationOnce(ImageSequence sqImg_, Point p_, unsigned nTime, bool bCenter_, SP<Event> pEvTerminate_)
        :bDead(false), t(nTime), p(p_), sqImg(sqImg_), pEvTerminate(pEvTerminate_), bCenter(bCenter_)
    {
        t2 = Timer(sqImg.GetTime());
    }
    
    /*virtual*/ void Update(MyCamera& cc, SP<SoundManager> pSnd);
};

class AnimationTimer: public Animation
{
    SP<Event> pEvTerminate;

protected:
    bool bDead;

    Point p;

    Timer t;
    Timer t2;

    Timer tTerm;
    ImageSequence sqImg;

    bool bCenter;

public:
    AnimationTimer(ImageSequence sqImg_, Point p_, unsigned nTime, bool bCenter_, SP<Event> pEvTerminate_, unsigned nTotalTime)
        :bDead(false), t(nTime), p(p_), sqImg(sqImg_), pEvTerminate(pEvTerminate_), bCenter(bCenter_), tTerm(nTotalTime)
    {
        t2 = Timer(sqImg.GetTime());
    }
    
    /*virtual*/ void Update(MyCamera& cc, SP<SoundManager> pSnd);
};

class SoundOnce: public Animation
{
    SP<Event> pEvTerminate;

protected:
    bool bDead;

    Timer t;
    Timer t2;
    SoundSequence sqSnd;

public:
    SoundOnce(SoundSequence sqSnd_, unsigned nTime, SP<Event> pEvTerminate_)
        :bDead(false), t(nTime), sqSnd(sqSnd_), pEvTerminate(pEvTerminate_)
    {
		std::cout << "magic debug statement to fix things" << std::endl;
        t2 = Timer(sqSnd.GetTime());
		std::cout << "magic debug statement to fix things" << std::endl;
    }
    
    
    /*virtual*/ void Update(MyCamera& cc, SP<SoundManager> pSnd);
};

struct Background
{
    Rectangle rBound;
    Color c;

    Background(Rectangle rBound_, Color c_ = Color())
        :rBound(rBound_), c(c_){}

    void Draw(MyCamera& cc){cc.pGr->DrawRectangle(rBound, c, false);}
};

struct MenuEntry
{
    std::string strText;
    SP<Event> pClick;

	Rectangle r;

    MenuEntry(std::string strText_, SP<Event> pClick_)
        :strText(strText_), pClick(pClick_){}
};

struct MenuSounds
{
	SP<Event> UpSound;
	SP<Event> DownSound;
	SP<Event> LeftSound;
	SP<Event> RightSound;
	SP<Event> InSound;
	SP<Event> OutSound;

	MenuSounds():
		UpSound(0), DownSound(0), LeftSound(0), RightSound(0), InSound(0), OutSound(0){}
	MenuSounds(SP<Event> UpSound_, SP<Event> DownSound_, SP<Event> InSound_ = 0, SP<Event> OutSound_ = 0):
		UpSound(UpSound_), DownSound(DownSound_), LeftSound(0), RightSound(0), InSound(InSound_), OutSound(OutSound_){}
	MenuSounds(SP<Event> UpSound_, SP<Event> DownSound_, SP<Event> LeftSound_, SP<Event> RightSound_, SP<Event> InSound_, SP<Event> OutSound_):
		UpSound(UpSound_), DownSound(DownSound_), LeftSound(LeftSound_), RightSound(RightSound_), InSound(InSound_), OutSound(OutSound_){}
	MenuSounds(SP<Event> MoveSound, SP<Event> InSound_ = 0, SP<Event> OutSound_ = 0):
		UpSound(MoveSound), DownSound(MoveSound), LeftSound(MoveSound), RightSound(MoveSound), InSound(InSound_), OutSound(OutSound_){}
};

class SubMenu : public GameController
{
	public:	
	MenuSounds msnd;

	void OnKey(GuiKeyType nCode, bool bUp)
	{
		if(nCode == GUI_ESCAPE)
		{
			Trigger(msnd.OutSound);
		}
	}
};

struct ActualGame;

struct HintsDisplay
{
	int nHint;
	
	std::vector<std::string> vHints;
	int nHintNum;

	HintsDisplay(){}
	
	HintsDisplay(GAME_DEVICE gd)
	:nHintNum(0), nHint(0)
	{
		if(gd == GD_PC)
			vHints.push_back("Press space to turn invisible");
		else if(gd == GD_MOBILE)
			vHints.push_back("Tap screen to turn invisible");
		else
			vHints.push_back("Press button to turn invisible");
		
		vHints.push_back("You can cross yourself while invisible");
		vHints.push_back("Eating mice fast gives extra score");
		vHints.push_back("Standing in place too long hurts you");
		vHints.push_back("Beating entire stage grants extra lives");
		vHints.push_back("On some levels you can wrap around the screen");
	}

	std::string GetHint()
	{
		return "Hint: " + vHints[nHintNum];
	}

	void RandomHint()
	{
		if(nHint == 0)
			nHintNum = 0;
		else
			nHintNum = rand()%vHints.size();

		++nHint;
	}
};

struct HintChangeEvent: public Event
{
	HintsDisplay& hd;

	HintChangeEvent(HintsDisplay& hd_):hd(hd_){}

	/*virtual*/ void Trigger()
	{
		hd.RandomHint();
	}
};

class MenuController: public GameController
{
    MyCamera cc;
    SP<FontWriter> pFnt;
    SP<Event> pExitEvent;
	MenuSounds msnd;
    
    std::vector<MenuEntry> vEntry;
    int nPos;
    Index imgPnt;

    Background bkgr;

	SP<ActualGame> pBckgrGame;

	bool bTimeTransition;
	Timer tTransition;

	bool bEsc;

public:

	bool bHints;
	HintsDisplay hd;
    SP<FontWriter> pHntFnt;

    MenuController(MyCamera cc_, SP<FontWriter> pFnt_, Index imgPnt_, Background bkgr_, SP<Event> pExitEvent_, bool bEsc_, MenuSounds msnd_ = MenuSounds(), SP<ActualGame> pBckgrGame_ = 0)
        :cc(cc_), pFnt(pFnt_), imgPnt(imgPnt_), nPos(0), bkgr(bkgr_), pExitEvent(pExitEvent_), msnd(msnd_), pBckgrGame(pBckgrGame_), bTimeTransition(false), bEsc(bEsc_), bHints(false)
	{
	}

    void AddEntry(std::string str, SP<Event> pClick);

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
	/*virtual*/ void OnClick(Point p);
};

class HighScoreController: public SubMenu
{
    SP<FontWriter> pFnt;
    MyCamera cc;
    Background bkgr;

    FilePath fp;

public:
    unsigned nScore;

    SP<Event> pTerminate;

    HighScoreController(SP<FontWriter> pFnt_, MyCamera cc_, SP<Event> pTerminate_, unsigned nScore_, Background bkgr_, FilePath fp_)
        :cc(cc_), pFnt(pFnt_), pTerminate(pTerminate_), nScore(nScore_), bkgr(bkgr_), fp(fp_)
    {
        Read();
    }

    ~HighScoreController()
    {
        Write();
    }

    void Read();
    void Write();

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
	/*virtual*/ void OnClick(Point p);
};

struct NewGameEvent;

class CutSceneController: public GameController
{
    SP<FontWriter> pFnt;
    MyCamera cc;
    Background bkgr;
    MyPreloader& pr;
    int nEntry;

	SP< SoundManager > pSndMng;
	bool bFirst;
	bool bNoSound;

    std::string sMessage;

	Timer tTerminate;

public:

    SP<Event> pTerminate;
	
	SP<NewGameEvent> pNewGameAux;

    CutSceneController(SP< SoundManager > pSndMng_, SP<FontWriter> pFnt_, MyCamera cc_, SP<Event> pTerminate_, Background bkgr_, int nEntry_, MyPreloader& pr_, bool bNoSound_ = false);
    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
    /*virtual*/ void OnClick(Point p);

	void SetNewGameEvent(SP<NewGameEvent> pNewGameAux_)
	{
		pNewGameAux = pNewGameAux_;
		pTerminate = pNewGameAux_;
	}
};



template<class T> 
class Matrix
{
    Size sz;
    std::vector<T> v;
public:
    Matrix(Size sz_):sz(sz_), v(sz_.x * sz_.y){}

	T& operator [](Point p){return v[p.x + p.y * sz.x];}
    T& at(Point p){return v.at(p.x + p.y * sz.x);}

	Size GetSize() const {return sz;}
};

struct Mouse;

struct ScreenObject
{
    bool bDraw;
    ImageSequence img;

    bool bSolid;
    int nSnakeLayers;
    Mouse* pMouse;

	bool bCenter;

    ScreenObject():bSolid(false), bDraw(false), nSnakeLayers(0), pMouse(0), bCenter(false){}

    void SetImage(Index img_, bool bSolid_ = false){img = ImageSequence(img_); bDraw = true; bSolid = bSolid_;}
    void UnSetImage(){bDraw = false; bSolid = false; img = ImageSequence();}

    virtual void Draw(MyCamera cc, Point p);
};

struct Grid
{
    MyCamera cc;

    Size szTotal;
    Size szElement;

    Matrix<ScreenObject> mtx;

	Grid(MyCamera cc_, Size szTotal_, Size szElement_)
        :cc(cc_), szTotal(szTotal_), szElement(szElement_), mtx(szTotal_){}

    void Set(Point p, Index img, bool bSolid = false);
    void UnSet(Point p);

    bool Draw();

    void Teleport(Point& p);
	
	public:
	int nMiceRemaining();
};

/*
struct LevelDefinition
{
    unsigned nMice;
    float dMiceRate;
    unsigned nMiceDistance;
    unsigned nBoulders;
    unsigned nSpeed;

    int nWallCode;

	Matrix<int> mtxObjects;
	LevelDefinition():mtxObjects(Size()){}

	void ReadObjectData(Image* pImg)
	{
		Size sz = pImg->GetSize();
		mtxObjects = Matrix<int>(sz);

		Point p;
		for(p.x = 0; p.x < sz.x; ++p.x)
		for(p.y = 0; p.y < sz.y; ++p.y)
			mtxObjects.at(p) = false;
	}
};
*/

inline Point GetDir(int n)
{
	if(n == 0)
		return Point(0, -1);
	if(n == 1)
		return Point(0, 1);
	if(n == 2)
		return Point(-1, 0);
	//if(n == 3)
		return Point(1, 0);
}

struct MiceDescr
{
	int nWallCode;
	int nRange;
	double dRate;

	float fMaxSpeed;
	float fRandomWalk;

	int nRndNum;

	MiceDescr():nRndNum(0){}
};

struct LevelDefinition
{
	unsigned nLength;
	unsigned nDisplayLength;
	unsigned nDir; // 0 - goes up, 1 - goes down, 2 - goes left, 3 - goes right
	Point pInPos;

	unsigned nSpeed;
	bool bInv;
	
	int nRndWall;

	bool bRndBoulders;
	bool bRndMice;

	LevelDefinition():mtxObjects(Size()), nRndWall(0), nDisplayLength(0), nDir(3), bRndBoulders(false), bRndMice(false){}

	Matrix<int> mtxObjects;
	std::vector<MiceDescr> vMice;

	void ReadSecondary(MyCamera cc, FilePath& fp, std::string sMiceFile, std::string sPicFile);
};



struct LevelInfo
{
	std::map<int, std::vector<LevelDefinition> > mpAllLevels;

    LevelDefinition lvCurr;
    
    unsigned nScore;
    unsigned nLives;

	unsigned nInitialLives;

    unsigned nLevel;

    bool LoadLevel(unsigned nNewLevel);
    //unsigned LengthRecursive(unsigned nNewLevel);
    void Read(MyCamera cc, FilePath& fp, std::string sSuffix);
};

struct ActualGame;

struct SnakeImages
{
	std::set<std::string> stImg;
};

struct Snake: virtual public SP_Info
{
	std::map<std::string, Index> mpSnakeImages;

	Timer tMove;
    Timer tAlternate;
	Timer tStillCounter;

	bool bRed;
	Timer tRedTimer;
	#define FLICKER_DURATION 10
	int m_invisibleFlickerTime;
	
	int m_deathAnimCounter;
	int m_deathAnimState;

	int m_damageFudge; //add a sligt delay before damage actually taken
	bool bDamageFudgeCnt;

#define STATE_PRE_DEATH_PAUSE 0
#define STATE_DISENTGRATE_BODY 1
#define STATE_PRE_DISINTEGRATE_HEAD 2
#define STATE_POST_DEATH_PAUSE 3
#define STATE_DEATH_DONE 4

//Death animation timing:
//frames between segment erasure
#define DEATH_STEP_PAUSE 3	

//pause before any erasing
#define PRE_DEATH_ANIM_PAUSE (15)
	
//pause before erasing the last segment, the skull
#define PRE_SKULL_PAUSE (25)

#define POST_DEATH_PAUSE_LENGTH (25)



    bool bBoomLock;
    
    std::list<Point> lsPos;
    Point pDir;
    Point pNextDir;

    unsigned nGrow;

    bool bInvisible;
    
    ActualGame* pGm;

    Snake(ActualGame* pGm_, SnakeImages snDt, Color c);

    bool Move();
    void Draw();
	void stepDeathAnimation();
	bool deathAnimationFinished()
	{
		return m_deathAnimState == STATE_DEATH_DONE; 
			//m_deathAnimCounter > PRE_DEATH_ANIM_PAUSE + lsPos.size() * DEATH_STEP_LENGTH + PRE_SKULL_PAUSE;
	}
	int nSkipSegments()
	{
		if (m_deathAnimState == STATE_DISENTGRATE_BODY)
		{
			return m_deathAnimCounter / DEATH_STEP_PAUSE;
		}
		else if (m_deathAnimState == STATE_PRE_DISINTEGRATE_HEAD)
		{
			return (lsPos.size() - 1);
		}
		else if (m_deathAnimState == STATE_POST_DEATH_PAUSE || m_deathAnimState == STATE_DEATH_DONE)
		{
			return lsPos.size();
		}
		else
		{
			return 0;
		}
	}

	int getDelayToDamage();

private:
	std::string getSnakeChar(std::list<Point>::iterator b, std::list<Point>::iterator f);
};

struct Mouse: virtual public SP_Info
{
    Point p;
	
	fPoint fFloatPos;
	fPoint fFloatVel;
	float fMaxSpeed;
	float fRandomWalk;

    Grid& gd;
    double dProb;

    int nDist;
	int nWallCode;

    bool bDead;

    MyPreloader& pr;

    std::string sPic;
    bool bPicToggle;

    Mouse(Point p_, double dProb_, Grid& gd_, MyPreloader& pr_, int nDist_, int nWallCode_, float fMaxSpeed_, float fRandomWalk_)
        :p(p_), gd(gd_), dProb(dProb_), pr(pr_), bDead(false), nDist(nDist_), sPic("mouse_right_0"),
		bPicToggle(false), nWallCode(nWallCode_), fFloatPos(p), fFloatVel(0, 0), fMaxSpeed(fMaxSpeed_), fRandomWalk(fRandomWalk_)
    {
        gd.Set(p, pr[sPic]);
        gd.mtx.at(p).pMouse = this;
    }

    void Move(Point pSnake);
    void Move();
};

struct ReferenceList
{
    SP<GameController>* ppCurrControl;
    SP<GameController>  pMenuControl;
};

struct LevelProgress
{
    std::vector<unsigned> v;
    LevelProgress():v(3, 1){}
};

std::ostream& operator << (std::ostream& ostr, const LevelProgress& pr);
std::istream& operator >> (std::istream& istr, LevelProgress& pr);

struct LevelProgressUpdater
{
    int i;
    LevelProgress& pr;

    LevelProgressUpdater(int i_, LevelProgress& pr_):i(i_), pr(pr_){}

    void Update(unsigned nLevel)
    {
        if(pr.v.at(i) < nLevel)
            pr.v.at(i) = nLevel;
    }
};

struct LevelSelectController;
struct NewGameEvent2;



struct InfoPack
{
	SP<NewGameEvent2>* ppTwoPlayerGameEvent;
	GAME_DEVICE gd;

	SnakeImages snDt;
	bool bPlayer1;
	
	InfoPack(SP<NewGameEvent2>* ppTwoPlayerGameEvent_, GAME_DEVICE gd_, SnakeImages snDt_, bool bPlayer1_)
		:ppTwoPlayerGameEvent(ppTwoPlayerGameEvent_), gd(gd_), snDt(snDt_), bPlayer1(bPlayer1_){}
};

struct ActualGame: public GameController
{
    InfoPack ipck;
	
	SP<GameController>* ppCurrControl;
    SP<HighScoreController> pHigh;
    LevelSelectController* pSel;

    MyPreloader& pr;
    MyCamera cc;
    SP<FontWriter> pFont;
    SP<FontWriter> pFontCut;

    Size szTotal;
    Size szElement;
    
    Background bkgr;
    Grid gd;

    SP<Snake> pSn;

    std::vector< SP<Mouse> > vMice;

    SP<Event> pLoseEvent;

    LevelInfo lvl;
    LevelProgressUpdater lpu;

	int nCombo;
	int m_comboClock; //number of updates before combo resets
	int getComboTimeBonus(); //When you eat a mouse, your combo clock gets more time.  Add less time for bigger combos?

    SP< SoundManager > pSndMng;

	bool bPreProgrammed;
	std::vector<GuiKeyType> vKeys;
	size_t nKeyPos;

    ActualGame(MyCamera cc_, Size szElement_, Background bkgr_, MyPreloader& pr_,
        SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo lvl_, SP<Event> pLoseEvent_, SP<GameController>* ppCurrControl_,
        SP<HighScoreController> pHigh_, LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, InfoPack ipck_, bool bPreProgrammed_ = false);
	
	void InitializeFromLevel();

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
    /*virtual*/ void OnClick(Point p);

	enum TerminalState {TS_PLAYING, TS_LOSE, TS_WIN};
	
	TerminalState ts;
	Timer tTerminal;

	// Amount of time before moving on to the cutscene
	#define WIN_DELAY 75
	#define LOSE_DELAY 45
	//int mWinDelayCount;
	//int mLoseDelayCount;
	/*enum GameState
	{
		GAME_OVER,
		OTHER
	}
	GameState mState;*/
	//int mDelayCount;

    void Lose(bool bActual = true);
    void Win();

	void playEatSound(); //plays sound depending on how many mice left.
	void playEatSound(int index);

	SP<GameController> pDialogBox;
	bool bAtContinueScreen;

	Timer tFlashStart;
};

struct LoseGameEvent: public Event
{
	ActualGame* pGm;

	LoseGameEvent(ActualGame* pGm_): pGm(pGm_){}

    /*virtual*/ void Trigger()
	{
		pGm->Lose();
	}
};

struct NewGameEvent: public Event
{
    InfoPack ipck;
	
	SP<GameController>* ppCurrControl;
    SP<HighScoreController> pHigh;
    LevelSelectController* pSel;

    SP< SoundManager > pSndMng;

    MyCamera cc;
    Size szElement;
    Background bkgr;
    MyPreloader& pr;
    SP<FontWriter> pFont;
    SP<FontWriter> pFontCut;

    SP<Event> pLoseEvent;

    LevelInfo lvl;
    LevelProgressUpdater lpu;

    NewGameEvent(SP<GameController>* ppCurrControl_, SP<Event> pLoseEvent_,
        MyCamera cc_, Size szElement_, Background bkgr_,
        MyPreloader& pr_, SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo lvl_, SP<HighScoreController> pHigh_,
        LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, InfoPack ipck_)
        :cc(cc_), szElement(szElement_), bkgr(bkgr_), pr(pr_), pFont(pFont_), pFontCut(pFontCut_), lvl(lvl_),
        ppCurrControl(ppCurrControl_), pLoseEvent(pLoseEvent_), pHigh(pHigh_), lpu(lpu_), pSel(pSel_), pSndMng(pSndMng_), ipck(ipck_){}

	
	/*virtual*/ void Trigger()
    {
		lvl.LoadLevel(lvl.nLevel);
		*ppCurrControl = new ActualGame(cc, szElement, bkgr, pr, pFont, pFontCut, lvl, pLoseEvent, ppCurrControl, pHigh, lpu, pSel, pSndMng, ipck);
    }

};

struct SelectElement
{
    bool bSelected;
    Color cDef;
    Color cSel;

    Point p;
    Size sz;

	bool bSimple;

    std::string sText;

    SelectElement():bSelected(false), bSimple(false){}

    void Draw(SP<FontWriter> pFont, MyCamera cc);

	Rectangle r;
};

struct SelectGrid
{
    Size sz;
    Matrix<SelectElement> mtxEl;

    Point pPos;
    Size szSpacing;

    SP<FontWriter> pFont;
    MyCamera cc;

    SelectGrid(Size sz_, Point pPos_, Size szSpacing_, SP<FontWriter> pFont_, MyCamera cc_);
    void Space();

    void Draw();
};



struct LevelSelectController: public GameController
{
    LevelInfo lvlE;
    LevelInfo lvlM;
    LevelInfo lvlH;

    LevelProgress& lp;

    SP<Event> pExitEvent;
    SP<NewGameEvent> pNewGameEvent;

    SelectGrid gr;

    Background bkgr;

    SP<FontWriter> pFont;
    MyCamera cc;

    Point pPos;

    FilePath& fp;

	MenuSounds msnd;

	bool bEsc;

	GAME_DEVICE gd;

    LevelSelectController(SP<Event> pExitEvent_, SP<NewGameEvent> pNewGameEvent_,
        SP<FontWriter> pFont_, Background bkgr_, FilePath& fp_, LevelProgress& lp_, MyCamera cc_, MenuSounds msnd_, bool bEsc_, GAME_DEVICE gd_);

    ~LevelSelectController();

    /*virtual*/ void Update();
    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void OnClick(Point p);
};

class ControlsController: public SubMenu
{
    SP<FontWriter> pFnt;
    MyCamera cc;
    Background bkgr;

    SP<Event> pTerminate;
public:

    int n;

    ControlsController(SP<FontWriter> pFnt_, SP<Event> pTerminate_, Background bkgr_, MyCamera cc_)
        :pFnt(pFnt_), pTerminate(pTerminate_), bkgr(bkgr_), n(rand()%3), cc(cc_){}

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};

class ControlsSwitchEvent : public Event
{
    SP<GameController>& objCurr;
    SP<ControlsController>& objNew;
public:
    ControlsSwitchEvent(SP<GameController>& objCurr_, SP<ControlsController>& objNew_)
        :objCurr(objCurr_), objNew(objNew_){}
    /*virtual*/ void Trigger()
    {
        objCurr = objNew;
        objNew->n = rand()%3;
    }
};



class CreditsController: public SubMenu
{
    SP<FontWriter> pFnt;
    MyCamera cc;
    Background bkgr;

    SP<Event> pTerminate;
public:

    CreditsController(SP<FontWriter> pFnt_, SP<Event> pTerminate_, Background bkgr_, MyCamera cc_)
        :pFnt(pFnt_), pTerminate(pTerminate_), bkgr(bkgr_), cc(cc_){}

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
	/*virtual*/ void OnClick(Point p);
};

class SnakeGameGlobalController: public GlobalController
{
    GAME_DEVICE gd;
	
	SP<Event> pExitProgram;

    SP< GraphicalInterface<Index> > pGr;
    SP< SoundManager > pSndMng;

    FilePath fp;
    SP<MyPreloader> pPr;

    SP<FontWriter> pFont;
    SP<FontWriter> pFontBig;

    SP<GameController> pCurrControl;

    SP<MenuController> pMenu;
    SP<HighScoreController> pHigh;
    SP<ControlsController> pCntr;
    SP<MenuController> pDiffMenu;

    LevelProgress lp;

    SP<LevelSelectController> pSel;
    SP<CreditsController> pCred;

    SP<EventAnimationController> pInit3;
    SP<EventAnimationController> pInit2;
    SP<AnbopluLogoController> pInit0;

    SP<CutSceneController> pCutE;
    SP<CutSceneController> pCutM;
    SP<CutSceneController> pCutH;

	SP<NewGameEvent2> pTwoPlayerGameEvent;

	SP<EventAnimationController> pArcadeIntro;

public:
    SnakeGameGlobalController(ProgramEngine pe);
	~SnakeGameGlobalController();

    /*virtual*/ void Update();
    
    /*virtual*/ void KeyDown(GuiKeyType nCode);
    /*virtual*/ void KeyUp(GuiKeyType nCode);
    /*virtual*/ void MouseClick(Point p);
};

struct BackgroundMusicPlayer
{
	int nCurrTheme;
	std::vector<Index> vThemes;
	SP<SoundManager> pSnd;

	BackgroundMusicPlayer():nCurrTheme(-1), pSnd(0){}

	void SwitchTheme(int nTheme);
	void StopMusic();
};

extern BackgroundMusicPlayer plr;

enum {BG_BACKGROUND = 0, BG_MENU = 1};

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Two-player code
/////////////////////////////////////////////////////////////////////////////////////////////////////

struct ActualGame2;
struct Snake2;

struct LevelDefinition2
{
	unsigned nLength;
	unsigned nDisplayLength;
	unsigned nDir; // 0 - goes up, 1 - goes down, 2 - goes left, 3 - goes right
	Point pInPos;

	unsigned nDir2;
	Point pInPos2;

	unsigned nSpeed;
	bool bInv;
	
	int nRndWall;

	bool bRndBoulders;
	bool bRndMice;

	LevelDefinition2():mtxObjects(Size()), nRndWall(0), nDisplayLength(0), nDir(3), bRndBoulders(false), bRndMice(false){}

	Matrix<int> mtxObjects;
	std::vector<MiceDescr> vMice;

	void ReadSecondary(MyCamera cc, FilePath& fp, std::string sMiceFile, std::string sPicFile);
};



struct LevelInfo2
{
	std::map<int, std::vector<LevelDefinition2> > mpAllLevels;

    LevelDefinition2 lvCurr;
    
    unsigned nScore;
    unsigned nLives;

    unsigned nLevel;

    bool LoadLevel(unsigned nNewLevel);
    //unsigned LengthRecursive(unsigned nNewLevel);
    void Read(MyCamera cc, FilePath& fp, std::string sSuffix);
};

struct PointToMove
{
	bool bVisible;
	bool bTail;
	int nTargetSnake;
	
	
	PointToMove(){}
	PointToMove(bool bVisible_, bool bTail_, int nTargetSnake_)
		:bVisible(bVisible_), bTail(bTail_), nTargetSnake(nTargetSnake_){}
};

struct SnakeMoveDescriptor
{
	SP<Snake2> pSn;

	bool bMoving;

	Point p;
	std::vector<PointToMove> vMoves;

	bool bUnclear;
	bool bLose;
	bool bTailMovement;

	bool bInternalProcessed;

	SnakeMoveDescriptor(SP<Snake2> pSn_, bool bMoving_):pSn(pSn_), bMoving(bMoving_),
		bUnclear(true), bLose(false), bTailMovement(false), bInternalProcessed(false){}

	void ProcessPoint(std::vector<SP<Snake2> >& vSnakes, std::vector<SnakeMoveDescriptor>& vSnakeDescr);
	
	void ProcessTails(std::vector<SnakeMoveDescriptor>& vSnakeDescr);
};

void ProcessHeadCollisions(std::vector<SnakeMoveDescriptor>& vSnakeDescr);



struct Snake2: virtual public SP_Info
{
    Timer tMove;
	
	int m_deathAnimCounter;
	int m_deathAnimState;

	std::map<std::string, Index> mpSnakeImages;

#define STATE_PRE_DEATH_PAUSE 0
#define STATE_DISENTGRATE_BODY 1
#define STATE_PRE_DISINTEGRATE_HEAD 2
#define STATE_POST_DEATH_PAUSE 3
#define STATE_DEATH_DONE 4

//Death animation timing:
//frames between segment erasure
#define DEATH_STEP_PAUSE 3	

//pause before any erasing
#define PRE_DEATH_ANIM_PAUSE (15)
	
//pause before erasing the last segment, the skull
#define PRE_SKULL_PAUSE (25)

#define POST_DEATH_PAUSE_LENGTH (25)

	bool bDead;
	int nBoomCounter;
    
    std::list<Point> lsPos;
    
	Point pDir;
	std::list<Point> lsDirBuff;

    unsigned nGrow;

    bool bInvisible;

	//bool bKeyLock;
    
    ActualGame2* pGm;

    Snake2(ActualGame2* pGm_, int nNum, SnakeImages snDt, Color c);

    //bool Move();
    void Draw();

	Point GetMove();
	void DoMove(Point p, bool bIgnoreTailErase);

	void NewDir(Point pNewDir);
	
	
	void stepDeathAnimation();
	bool deathAnimationFinished()
	{
		return m_deathAnimState == STATE_DEATH_DONE; 
			//m_deathAnimCounter > PRE_DEATH_ANIM_PAUSE + lsPos.size() * DEATH_STEP_LENGTH + PRE_SKULL_PAUSE;
	}
	int nSkipSegments()
	{
		if (m_deathAnimState == STATE_DISENTGRATE_BODY)
		{
			return m_deathAnimCounter / DEATH_STEP_PAUSE;
		}
		else if (m_deathAnimState == STATE_PRE_DISINTEGRATE_HEAD)
		{
			return (lsPos.size() - 1);
		}
		else if (m_deathAnimState == STATE_POST_DEATH_PAUSE || m_deathAnimState == STATE_DEATH_DONE)
		{
			return lsPos.size();
		}
		else
		{
			return 0;
		}
	}

	int getDelayToDamage();

private:
	std::string getSnakeChar(Point b, Point f);
};

struct InfoPack2
{	
	GAME_DEVICE gd;
	
	InfoPack2(GAME_DEVICE gd_):gd(gd_){}
};

struct ActualGame2: public GameController
{
    InfoPack2 ipck;
	
	SP<GameController>* ppCurrControl;
    SP<HighScoreController> pHigh;
    LevelSelectController* pSel;

	SnakeImages snDt;

    MyPreloader& pr;
    MyCamera cc;
    SP<FontWriter> pFont;
    SP<FontWriter> pFontCut;

    Size szTotal;
    Size szElement;
    
    Background bkgr;
    Grid gd;

	std::vector< SP<Snake2> >  vSnakes;
    

    std::vector< SP<Mouse> > vMice;

    SP<Event> pLoseEvent;

    LevelInfo2 lvl;
    LevelProgressUpdater lpu;

	int nCombo;
	int m_comboClock; //number of updates before combo resets
	int getComboTimeBonus(); //When you eat a mouse, your combo clock gets more time.  Add less time for bigger combos?

    SP< SoundManager > pSndMng;

	bool bPreProgrammed;
	std::vector<GuiKeyType> vKeys;
	size_t nKeyPos;

    ActualGame2(MyCamera cc_, Size szElement_, Background bkgr_, MyPreloader& pr_,
        SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo2 lvl_, SP<Event> pLoseEvent_, SP<GameController>* ppCurrControl_,
        SP<HighScoreController> pHigh_, LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, SnakeImages snDt_, InfoPack2 ipck_, bool bPreProgrammed_ = false);
	
	void InitializeFromLevel();

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
	/*virtual*/ void OnClick(Point p);

    void OnKeySnake(GuiKeyType nCode, SP<Snake2> pCurrSn, bool bUp);

	enum TerminalState {TS_PLAYING, TS_LOSE, TS_WIN};
	
	TerminalState ts;
	Timer tTerminal;

	// Amount of time before moving on to the cutscene
	#define WIN_DELAY 75
	#define LOSE_DELAY 45

    void Lose(bool bActual = true);
    void Win();

	public:
	void playEatSound(); //plays sound depending on how many mice left.
	void playEatSound(int index);

	SP<GameController> pDialogBox;
};

struct NewGameEvent2: public Event
{
    InfoPack2 ipck;
	
	SP<GameController>* ppCurrControl;
    SP<HighScoreController> pHigh;
    LevelSelectController* pSel;

    SP< SoundManager > pSndMng;

    MyCamera cc;
    Size szElement;
    Background bkgr;
    MyPreloader& pr;
    SP<FontWriter> pFont;
    SP<FontWriter> pFontCut;

    SP<Event> pLoseEvent;

    LevelInfo2 lvl;
    LevelProgressUpdater lpu;

	SnakeImages snDt;

    NewGameEvent2(SP<GameController>* ppCurrControl_, SP<Event> pLoseEvent_,
        MyCamera cc_, Size szElement_, Background bkgr_,
        MyPreloader& pr_, SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo2 lvl_, SP<HighScoreController> pHigh_,
        LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, SnakeImages snDt_, InfoPack2 ipck_)
        :cc(cc_), szElement(szElement_), bkgr(bkgr_), pr(pr_), pFont(pFont_), pFontCut(pFontCut_), lvl(lvl_), ipck(ipck_),
        ppCurrControl(ppCurrControl_), pLoseEvent(pLoseEvent_), pHigh(pHigh_), lpu(lpu_), pSel(pSel_), pSndMng(pSndMng_), snDt(snDt_){}

    /*virtual*/ void Trigger()
    {
		lvl.LoadLevel(rand()%7  + 1);
		*ppCurrControl = new ActualGame2(cc, szElement, bkgr, pr, pFont, pFontCut, lvl, pLoseEvent, ppCurrControl, pHigh, lpu, pSel, pSndMng, snDt, ipck);
    }

};

class CutSceneController2: public GameController
{
    SP<FontWriter> pFnt;
    MyCamera cc;
    Background bkgr;
    MyPreloader& pr;
    int nEntry;

	SP< SoundManager > pSndMng;
	bool bFirst;
	bool bNoSound;

    std::string sMessage;

	bool bStart1;
	bool bStart2;

public:

    SP<Event> pTerminate;

    CutSceneController2(SP< SoundManager > pSndMng_, SP<FontWriter> pFnt_, MyCamera cc_, SP<Event> pTerminate_, Background bkgr_, int nEntry_, MyPreloader& pr_, bool bNoSound_ = false);
    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};


#endif  //SNAKETRIX_HEADER_08_12_10_09_39
