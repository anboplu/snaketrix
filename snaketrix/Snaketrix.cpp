#include "Snaketrix.h"

unsigned nFrameRate = 30;
bool bIgnoreSpaceReturn = true;

/*virtual*/ void AnimationController::Update()
{
    SP<AnimationController> pAn = this; //safeguard

	if(nTheme >= 0)
		plr.SwitchTheme(nTheme);

    if(!bStacked)
		cc.pGr->DrawRectangle(rBound, cBkg, false);
    
    for(std::list< SP<Animation> >::iterator itr = lsAnim.begin(), etr = lsAnim.end(); itr != etr; ++itr)
        (*itr)->Update(cc, pSnd);

    if(!bStacked)
		cc.pGr->RefreshAll();
}

/*virtual*/ void EventAnimationController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(bUp)
        return;

	std::map<GuiKeyType, SP<Event> >::iterator itr = mpKeyEvents.find(nCode);

	if(itr != mpKeyEvents.end())
	{
		Trigger(itr->second);
		return;
	}


    if(!bEsc || nCode == GUI_ESCAPE)    
        Terminate();
}

/*virtual*/ void EventAnimationController::OnClick(Point p)
{
    Terminate();
}

/*virtual*/ void StaticImage::Update(MyCamera& cc, SP<SoundManager> pSnd)
{
    cc.DrawImage(p, img, bCenter);
}   


/*virtual*/ void AnimationOnce::Update(MyCamera& cc, SP<SoundManager> pSnd)
{
    if(bDead)
        return;
    
    if(t.Tick() && t2.Tick())
    {
        if(sqImg.Toggle())
        {
            bDead = true;
            Trigger(pEvTerminate);
            return;
        }

        t2 = Timer(sqImg.GetTime());
    }

    cc.DrawImage(p, sqImg.GetImage(), true);
}

/*virtual*/ void AnimationTimer::Update(MyCamera& cc, SP<SoundManager> pSnd)
{
    if(bDead)
        return;
    
    if(tTerm.Tick())
    {
        bDead = true;
        Trigger(pEvTerminate);
        return;
    }

    if(t.Tick() && t2.Tick())
    {
        sqImg.Toggle();
        t2 = Timer(sqImg.GetTime());
    }

    cc.DrawImage(p, sqImg.GetImage(), true);
}

/*virtual*/ void SoundOnce::Update(MyCamera& cc, SP<SoundManager> pSnd)
{
    if(bDead)
        return;
    
    if(t.Tick() && t2.Tick())
    {
        pSnd->PlaySound(sqSnd.GetSound());
        if(sqSnd.Toggle())
        {
            bDead = true;
            Trigger(pEvTerminate);
            return;
        }

        t2 = Timer(sqSnd.GetTime());
    }
}

void MenuController::AddEntry(std::string str, SP<Event> pClick)
{
    vEntry.push_back(MenuEntry(str, pClick));
}

/*virtual*/ void MenuController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(vEntry.empty())
        return;

    if(bUp)
        return;

	if(bTimeTransition)
		return;

    if(nCode == GUI_ESCAPE)
    {
		Trigger(msnd.OutSound);
        Trigger(pExitEvent);
		return;
    }


	if(!bEsc)
	{
		if(nCode == GUI_UP)
		{
			Trigger(msnd.UpSound);
			--nPos;
			if(nPos < 0)
				nPos = vEntry.size() - 1;
		}
		else if(nCode == GUI_DOWN)
		{
			Trigger(msnd.DownSound);
			++nPos;
			if(unsigned(nPos) >= vEntry.size())
				nPos = 0;
		}

		if(nCode == GUI_RETURN || nCode == GuiKeyType(' ') || nCode == GUI_F1)
		{
			Trigger(msnd.InSound);
			Trigger(vEntry.at(nPos).pClick);
		}
	}
}

/*virtual*/ void MenuController::Update()
{
    bkgr.Draw(cc);

	plr.SwitchTheme(BG_MENU);

    if(vEntry.empty())
        return;

	if(pBckgrGame != 0)
		pBckgrGame->Update();

	if(bTimeTransition && tTransition.Tick())
	{
		bTimeTransition = false;
		Trigger(msnd.InSound);
		Trigger(vEntry.at(nPos).pClick);
		return;
	}
	
	cc.Push();

	
    Size szSymbol = cc.fromR(pFnt->szSymbol);
	cc.Translate(fPoint(.5 - szSymbol.x * (vEntry.size() - 1.5)/(float)cc.GetBox().x/2. - vEntry.size() * 5 / (float)cc.GetBox().x , .5));

    cc.Translate(Point(0, - szSymbol.y * int(vEntry.size()) / 2));

    for(unsigned i = 0; i < vEntry.size(); ++i)
    {
        pFnt->DrawWord(vEntry[i].strText, cc.toR(Point()));

		vEntry[i].r = Rectangle(cc.toR(Point()), pFnt->GetSize(vEntry[i].strText));

        if(i == nPos)
        {
            Size sz = cc.fromR(cc.pGr->GetImage(imgPnt)->GetSize());
            cc.DrawImage(Point( - sz.x * 3 / 2, 0 ) , imgPnt);
        }

        cc.Translate(Point(szSymbol.x, szSymbol.y * 4 / 5));
    }

    cc.Pop();

	if(bHints)
	{
		std::string s = hd.GetHint();

		cc.Push();

#ifdef 	CUTE_AND_COMFY
		cc.Translate(cc.fromF(fPoint(.3, .95)));
#else
		cc.Translate(cc.fromF(fPoint(.5, .8)));
#endif

		pHntFnt->DrawWord(s, cc.toR(Point()), true);
		
		cc.Pop();
	}

    cc.pGr->RefreshAll();
}

/*virtual*/ void MenuController::OnClick(Point p)
{
    //cc.Push();

    //cc.Translate(fPoint(.35, .55));
    //Size szSymbol = cc.fromR(pFnt->szSymbol);
    //cc.Translate(Point(0, - szSymbol.y * int(vEntry.size()) / 2));

    for(unsigned i = 0; i < vEntry.size(); ++i)
    {
		//Rectangle rWord = cc.fromR(pFnt->GetSize(vEntry[i].strText));
		if(InsideRectangle(vEntry[i].r, p))//cc.fromR(p)))
		{
			nPos = i;
			bTimeTransition = true;
			tTransition = Timer(6);
			break;
		}

        //cc.Translate(Point(szSymbol.x, szSymbol.y * 4 / 5));
    }

   // cc.Pop();
}



void HighScoreController::Read()
{
    std::string sFile = "high.txt";
    fp.Parse(sFile);

	SP<InStreamHandler> pFl = fp.ReadFile(sFile);
	std::istream& ifs = pFl->GetStream();
	
    ifs >> nScore;
}

void HighScoreController::Write()
{
    std::string sFile = "high.txt";
    fp.Parse(sFile);
	
	SP<OutStreamHandler> pFl = fp.WriteFile(sFile);
	std::ostream& ofs = pFl->GetStream();
    
	ofs << nScore;
}

/*virtual*/ void HighScoreController::OnKey(GuiKeyType nCode, bool bUp)
{
	SubMenu::OnKey(nCode,  bUp);
    if(!bUp)
        Trigger(pTerminate);
}

/*virtual*/ void HighScoreController::OnClick(Point p)
{
	Trigger(msnd.OutSound);
	Trigger(pTerminate);
}


/*virtual*/ void HighScoreController::Update()
{
    bkgr.Draw(cc);

    Size sz = cc.fromR(pFnt->szSymbol);
    
    cc.Push();
    
    cc.Translate(fPoint(.5, .5));
    
    pFnt->DrawWord(pFnt->GetNumber(nScore), cc.toR(Point()), true, false);

    cc.Translate(Point(0, - sz.y));
    
    pFnt->DrawWord("High Score", cc.toR(Point()), true, false);

    cc.Pop();

    cc.pGr->RefreshAll();
}

CutSceneController::CutSceneController(SP< SoundManager > pSndMng_, SP<FontWriter> pFnt_, MyCamera cc_, SP<Event> pTerminate_, Background bkgr_, int nEntry_, MyPreloader& pr_, bool bNoSound_)
    :cc(cc_), pFnt(pFnt_), pTerminate(pTerminate_), bkgr(bkgr_), pr(pr_), nEntry(nEntry_), pSndMng(pSndMng_), bFirst(true), bNoSound(bNoSound_), tTerminate(30*5)
{
    if(nEntry == 0)
        sMessage = "My name is Edora and there are/but three left of our kind";
    else if(nEntry == 1)
        sMessage = "We were once more numerous than/the leaves of an empress tree";
    else if(nEntry == 2)
        sMessage = "Proud and noble species we were";
    else if(nEntry == 3)
        sMessage = "Our land once green and lush...";
    else if(nEntry == 4)
        sMessage = "Whithered now,/this barren cold oasis";
    else if(nEntry == 5)
        sMessage = "My name is Midora and there are/but two left of our kind";
    else if(nEntry == 6)
        sMessage = "Misguided by our prosperity,/we lost our old ways";
    else if(nEntry == 7)
        sMessage = "Our arrogance blinded us/from our imminent downfall";
    else if(nEntry == 8)
        sMessage = "The prudent few,/we shunned";
    else if(nEntry == 9)
        sMessage = "All great empires must fall";
    else if(nEntry == 10)
        sMessage = "I am Hidora and I am/the last of my kind";
    else if(nEntry == 11)
        sMessage = "I travel these lands and/ponder the meaning of my existence";
    else if(nEntry == 12)
        sMessage = "Hate is all I feel";
    else if(nEntry == 13)
        sMessage = "Yet I have only myself to blame";
    else if(nEntry == 14)
        sMessage = "I can no longer bear this burden";
    else if(nEntry == 15)
        sMessage = "If only we had acted sooner";
	else
		sMessage = "";
}

void CutSceneController::OnKey(GuiKeyType nCode, bool bUp)
{
	if(!bUp)
        if(nCode == ' ' || nCode == GUI_ESCAPE)
		{
            tTerminate = Timer(30*5);
			Trigger(pTerminate);
		}
}

void CutSceneController::OnClick(Point p)
{
	Trigger(pTerminate);
}


void SeparateMessage(std::string sMsg, std::string& sRes1, std::string& sRes2)
{
    sRes1 = sRes2 = "";
    
    bool bSep = false;
    for(unsigned i = 0; i < sMsg.length(); ++i)
        if(sMsg[i] == '/')
            bSep = true;
        else
        {
            if(bSep)
                sRes2 += sMsg[i];
            else
                sRes1 += sMsg[i];
        }
}

void CutSceneController::Update()
{
    bkgr.Draw(cc);

	plr.StopMusic();

	if(bFirst)
	{
		bFirst = false;
		if(!bNoSound)
		{
			if(nEntry <= 15)
				pSndMng->PlaySound(pr.GetSnd("cutscene_" + S((4 + nEntry-1)%4 + 1)));
			else
			{
				// Alex insert sound here - depending on the final screen (16, 17, or 18)
			}
		}
	}

    Size sz = cc.fromR(pFnt->szSymbol);

    std::string s1, s2;

    SeparateMessage(sMessage, s1, s2);
    
    cc.Push();
    
    cc.Translate(fPoint(.5, .5));

    int nNum = nEntry%5;
    if(nEntry == 15)
        nNum = 5;
    
    if(nEntry <= 15)
		cc.DrawImage(Point(), pr("border").vImage.at(nNum), true);
	else
	{
		if(nEntry == 16)
			cc.DrawImage(Point(), pr["death_1"], true);
		else if(nEntry == 17)
			cc.DrawImage(Point(), pr["death_2"], true);
		else
			cc.DrawImage(Point(), pr["death_3"], true);
	}

    if(s2 != "")
        cc.Translate(Point(0, -cc.fromR(pFnt->GetSize(s1)).y/2));
    
    pFnt->DrawWord(s1, cc.toR(Point()), true, false);

    cc.Translate(Point(0, cc.fromR(pFnt->GetSize(s1)).y));

    pFnt->DrawWord(s2, cc.toR(Point()), true, false);

    cc.Pop();

	cc.pGr->RefreshAll();

	if(tTerminate.Tick())
	{
		Trigger(pTerminate);
		return;
	}
}


void ScreenObject::Draw(MyCamera cc, Point p)
{
    if(bDraw)
	{
        if(!bCenter)
			cc.DrawImage(p, img.GetImage(), false, false);
		else
			cc.DrawImage(p, img.GetImage(), true, false);
	}
}

bool Grid::Draw()
{
    int nMice = 0;

    Point p;
    for(p.y = 0; p.y < szTotal.y; ++p.y)
    for(p.x = 0; p.x < szTotal.x; ++p.x)
    {
        if(mtx.at(p).pMouse)
            ++nMice;

		if (!(mtx.at(p).pMouse && mtx.at(p).pMouse->bDead)) //don't draw any dead mice
		{
			if(!mtx.at(p).bCenter)
				mtx.at(p).Draw(cc, Point(p.x * szElement.x, p.y * szElement.y));
			else
				mtx.at(p).Draw(cc, Point((p.x + .5)* szElement.x, (p.y + .5) * szElement.y));
		}
    }
    return nMice == 0;
}

// <ALEX CODE> //

//Probably a smarter way to do this, but.. I'm just going to leave it as is.
int Grid::nMiceRemaining()
{
	int nMice = 0;

	Point p;
	for(p.y = 0; p.y < szTotal.y; ++p.y)
		for(p.x = 0; p.x < szTotal.x; ++p.x)
		{
			if(mtx.at(p).pMouse) //&& !mtx.at(p).pMouse->bDead
				++nMice;
		}
		return nMice; 
}
// </ALEX CODE> //

void Grid::Teleport(Point& p)
{
    while(p.x < 0)
        p.x += szTotal.x;
    while(p.x >= szTotal.x)
        p.x -= szTotal.x;
    while(p.y < 0)
        p.y += szTotal.y;
    while(p.y >= szTotal.y)
        p.y -= szTotal.y;
}


bool LevelInfo::LoadLevel(unsigned nNewLevel)
{
    if(mpAllLevels.find(nNewLevel) == mpAllLevels.end() || mpAllLevels[nNewLevel].empty())
        return false;

    nLevel = nNewLevel;
    lvCurr = mpAllLevels[nNewLevel][rand()%mpAllLevels[nNewLevel].size()];
    
    return true;
}

/*
unsigned LevelInfo::LengthRecursive(unsigned nNewLevel)
{
    unsigned nTotalLength = nLength;
    for(unsigned i = 1; i < nNewLevel; ++i)
    {
        if(!LoadLevel(i))
            return 0;
        nTotalLength += lvCurr.nMice;
    }
    if(!LoadLevel(nNewLevel))
        return 0;
    return nTotalLength;  
}
*/


void LevelInfo::Read(MyCamera cc, FilePath& fp, std::string sSuffix)
{
    std::string s = "levelseq" + sSuffix + ".txt";
    fp.Parse(s);

	SP<InStreamHandler> pFl = fp.ReadFile(s);
	std::istream& ifs = pFl->GetStream();
    
    std::getline(ifs, s);

    //int nLength;
	//ifs >> nLength >> nScore >> nLives;
	ifs >> nLives;
	nInitialLives = nLives;
	nScore = 0;

	std::getline(ifs, s);
    
    while(true)
    {
		std::getline(ifs, s);
		if(ifs.fail())
			break;

		std::istringstream istr(s);

        int nLevel;
        LevelDefinition ld;

		//int nMice;
		//double dMiceRate;
		//int nMiceDistance;
		//int nBoulders;
		//int nWallCode;

        //ifs >> nLevel >> ld.nMice >> ld.dMiceRate >> ld.nMiceDistance >> ld.nBoulders >> ld.nSpeed >> ld.nWallCode;
        //ifs >> nLevel >> nMice >> dMiceRate >> nMiceDistance >> nBoulders >> ld.nSpeed >> nWallCode;
		istr >> nLevel >> ld.nLength >> ld.nSpeed >> ld.bInv;

		std::string sMiceFile;
		std::vector<std::string> vPicFiles;

		istr >> sMiceFile;
		while(true)
		{
			std::string sPicFile;
			istr >> sPicFile;
			if(istr.fail())
				break;
			vPicFiles.push_back(sPicFile);
		}

		for(unsigned j = 0; j < vPicFiles.size(); ++j)
		{
			LevelDefinition ldn = ld;
			ldn.ReadSecondary(cc, fp, sMiceFile, vPicFiles[j]);
			mpAllLevels[nLevel].push_back(ldn);
		}

		//ld.bInv = false;
		//ld.nLength = nLength;
		//ld.nDisplayLength = nLength;
		//ld.nDir = 2;
		//ld.pInPos = Point(10,10);

		//ld.mtxObjects = Matrix<int>(Size(10, 10));
		//ld.mtxObjects.at(Point(5,5)) = 1;
		
		//MiceDescr md;
		//md.dRate = .01;
		//md.nRange = 0;
		//md.nWallCode = 0;

		//ld.vMice.push_back(md);

    }
}

void LevelDefinition::ReadSecondary(MyCamera cc, FilePath& fp, std::string sMiceFile, std::string sPicFile)
{
	if(sPicFile.length() == 0)
		throw SimpleException("LevelDefinition", "ReadSecondary", "Empty file name!");
	
	if(sPicFile[0] == 'R')
		bRndBoulders = bRndMice = true;
	else if(sPicFile[0] == 'M')
		bRndMice = true;
	else if(sPicFile[0] == 'B')
		bRndBoulders = true;
	
	fp.Parse(sMiceFile);
	fp.Parse(sPicFile);

	std::map<Color, int> mpMiceNumByColor;

	{
		std::map<Color, MiceDescr> mpMiceByColor;

		SP<InStreamHandler> pFl = fp.ReadFile(sMiceFile);
		std::istream& ifs = pFl->GetStream();


		if(ifs.fail())
			throw SimpleException("LevelDefinition", "ReadSecondary", "Cannot open file " + sMiceFile);

		std::string s;
		std::getline(ifs, s);

		while(true)
		{
			MiceDescr md;
			int nR, nG, nB;

			ifs >> nR >> nG >> nB >> md.nWallCode >> md.nRange >> md.dRate >> md.fMaxSpeed >> md.fRandomWalk;

			Color c(nR, nG, nB);

			if(ifs.fail())
				break;
			mpMiceByColor[c] = md;
		}

		for(std::map<Color, MiceDescr>::iterator itr = mpMiceByColor.begin(), etr = mpMiceByColor.end(); itr != etr; ++itr)
		{
			mpMiceNumByColor[itr->first] = vMice.size();
			vMice.push_back(itr->second);
		}
	}

	{
		Index iImg = cc.pGr->LoadImage(sPicFile);
		Image* pImg = cc.pGr->GetImage(iImg);

		Point p;
		Size sz = pImg->GetSize();

		mtxObjects = Matrix<int>(sz);

		for(p.x = 0; p.x < sz.x; ++p.x)
		for(p.y = 0; p.y < sz.y; ++p.y)
		{
			Color c = pImg->GetPixel(p);
			if(c == Color(120, 120, 120))
				mtxObjects[p] = -1;
			else if(c == Color(255, 255, 255))
			{
				mtxObjects[p] = -2;
				++nRndWall;
			}
			else if(c == Color(168, 230, 29))
			{
				pInPos = p;
				++nDisplayLength;

				for(int nTDr = 0; nTDr < 4; ++nTDr)
				{
					Point pDr = -GetDir(nTDr);
					pDr = p + pDr;
					if(InsideRectangle(Rectangle(sz), pDr))
					{
						if(pImg->GetPixel(pDr) == Color(34,177,76))
							nDir = nTDr;
					}
				}
			}
			else if(c == Color(34,177,76))
			{
				++nDisplayLength;
			}
			else
			{
				std::map<Color, int>::iterator itr = mpMiceNumByColor.find(c);
				if(itr != mpMiceNumByColor.end())
				{
					mtxObjects[p] = itr->second + 1;
					++vMice[itr->second].nRndNum;
				}
				else
					mtxObjects[p] = 0;
			}
				
		}
	}
}



Snake::Snake(ActualGame* pGm_, SnakeImages snDt, Color c)
:pDir(GetDir(pGm_->lvl.lvCurr.nDir)), tMove(pGm_->lvl.lvCurr.nSpeed), bInvisible(false), nGrow(0), bBoomLock(false), pGm(pGm_), tAlternate(1), bRed(false), bDamageFudgeCnt(false)
{
    pNextDir = pDir;
	m_deathAnimCounter = 0;
	m_deathAnimState = STATE_PRE_DEATH_PAUSE;
	m_invisibleFlickerTime = FLICKER_DURATION;
	//m_delayToDamage = DELAY_TO_DAMAGE;
    
	unsigned nLength = pGm->lvl.lvCurr.nLength;
    
	if (nLength > pGm->lvl.lvCurr.nDisplayLength)
	{
		nGrow = nLength - pGm->lvl.lvCurr.nDisplayLength;
		nLength = pGm->lvl.lvCurr.nDisplayLength;
	}
    
	Point p1 = pGm->lvl.lvCurr.pInPos;
	Point p2 = -GetDir(pGm->lvl.lvCurr.nDir);

    for(unsigned i = 0; i < nLength; ++i)
    {
        ++pGm->gd.mtx.at(p1).nSnakeLayers;
        lsPos.push_back(p1);
		p1 += p2;
    }

	for(std::set<std::string>::iterator itr = snDt.stImg.begin(), etr = snDt.stImg.end(); itr != etr; ++itr)
	{
		std::string sName = *itr;
		Index img = pGm->pr[sName];
		Index imgCp = pGm->cc.pGr->CopyImage(img);
		pGm->cc.pGr->GetImage(imgCp)->ChangeColor(Color(0, 255, 0), c);

		mpSnakeImages[sName] = imgCp;
	}
}

int Snake::getDelayToDamage()
{
	//easy slow nSpeed ~= 5
	//medium fast nSpeed ~= 2
	return 7;//ceil(10.0 / pGm->lvl.lvCurr.nSpeed) + 1; //3 seconds, just to test accuracy
}

void Snake::stepDeathAnimation()
{
	m_deathAnimCounter++;
	if (
		m_deathAnimState == STATE_PRE_DEATH_PAUSE && m_deathAnimCounter == PRE_DEATH_ANIM_PAUSE
		||
		m_deathAnimState == STATE_DISENTGRATE_BODY && m_deathAnimCounter == (lsPos.size() - 1)*DEATH_STEP_PAUSE
		||
		m_deathAnimState == STATE_PRE_DISINTEGRATE_HEAD && m_deathAnimCounter == PRE_SKULL_PAUSE
		||
		m_deathAnimState == STATE_POST_DEATH_PAUSE && m_deathAnimCounter == POST_DEATH_PAUSE_LENGTH
		)
	{
		m_deathAnimCounter = 0;
		m_deathAnimState++;
	}

	if (m_deathAnimState == STATE_DISENTGRATE_BODY &&  m_deathAnimCounter%DEATH_STEP_PAUSE == DEATH_STEP_PAUSE - 1)
	{
		pGm->pSndMng->StopSound(5);
		pGm->pSndMng->PlaySound(pGm->pr.GetSnd("crumble"),5);
	} 
	else if (m_deathAnimState == STATE_POST_DEATH_PAUSE && m_deathAnimCounter == 0)
	{
		pGm->pSndMng->PlaySound(pGm->pr.GetSnd("deathsplosion"));
	}
	
}
bool Snake::Move()
{
	//TODO
	//new snake move
	//calculate position of the whole snake
	//derive new screen object class for the snake that takes setParam(glow,turn)
	//figure out which screen objects along the whole snake and put them onto the matrix
	//use timer to determine transparency and call set param as appropriate

	//Also... drawing really shouldn't be happening in this function.

	if(pGm->lvl.nLives == 0)
	{
		Draw();
		return true;
	}
	else
	{
		if(tAlternate.Tick())
		{
			for(std::list<Point>::iterator itr = lsPos.begin(), etr = lsPos.end(); itr != etr; ++itr)
				pGm->gd.mtx.at(*itr).img.Toggle();
		}

		if(bRed && tRedTimer.Tick())
		{
			bRed = false;
		}
	    
		if (m_damageFudge != 0 && bDamageFudgeCnt == true)
		{
			m_damageFudge--;
		}

		if(!tMove.Tick() && (bDamageFudgeCnt == false || m_damageFudge != 0))
			return false;

		if(pNextDir.x != pDir.x && pNextDir.y != pDir.y && !bBoomLock && m_damageFudge == getDelayToDamage())
		{
			if (pNextDir.x == -1)
			{
				pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn01"));
			} 
			else if (pNextDir.y == 1)
			{
				pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn02"));
			} 
			else if (pNextDir.x == 1)
			{
				pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn03"));
			} 
			else if (pNextDir.y == -1)
			{
				pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn04"));
			} 
		}

		pDir = pNextDir;
	    
		Point p = lsPos.front() + pDir;
	    


		pGm->gd.Teleport(p);

		if(bInvisible && pGm->gd.mtx.at(p).nSnakeLayers != 0)
		{
			pGm->pSndMng->PlaySound(pGm->pr.GetSnd("crossover"));

			//if(pGm->lpu.i == 2)
			//	pGm->nCombo = 0;
		}

		if((lsPos.back() == p) && (nGrow == 0))
		{
			lsPos.push_front(p);
			lsPos.pop_back();

			Draw();

			return false;
		}

		if(pGm->gd.mtx.at(p).bSolid || (!bInvisible && pGm->gd.mtx.at(p).nSnakeLayers != 0))
		{

			bDamageFudgeCnt = true;
			
			Point p1 = *lsPos.begin();
			Point p2 = *++lsPos.begin();

			pDir = p1 - p2;


			if(!bBoomLock && m_damageFudge == 0)
			{
				pGm->pSndMng->PlaySound(pGm->pr.GetSnd("die"));
				if(pGm->lvl.nLives == 0)
					return true;
				--pGm->lvl.nLives;
				bRed = true;
				
				if(tMove.nPeriod == 1)
					tRedTimer = Timer(4);
				else if(tMove.nPeriod == 2)
					tRedTimer = Timer(2);
				else
					tRedTimer = Timer(1);
			}

			if (m_damageFudge == 0)
			{
				if(bBoomLock == false)
				{
					tStillCounter = Timer(90);
				}

				bBoomLock = true;

				if(tStillCounter.Tick())
				{
					tStillCounter = Timer(45);
					pGm->pSndMng->PlaySound(pGm->pr.GetSnd("die"));
					if(pGm->lvl.nLives == 0)
						return true;
					--pGm->lvl.nLives;

					bRed = true;
					
					if(tMove.nPeriod == 1)
						tRedTimer = Timer(4);
					else if(tMove.nPeriod == 2)
						tRedTimer = Timer(2);
					else
						tRedTimer = Timer(1);
				}
			}

			Draw();

			return false;
		} 
		else 
		{
			m_damageFudge = getDelayToDamage();
			bDamageFudgeCnt = false;
		}

		if(pGm->gd.mtx.at(p).pMouse)
		{
			//pGm->pSndMng->PlaySound(pGm->pr.GetSnd("mouse"));

			pGm->playEatSound(pGm->nCombo + 1);
			
			pGm->m_comboClock += pGm->getComboTimeBonus();


			pGm->gd.mtx.at(p).pMouse->bDead = true;
			pGm->gd.mtx.at(p).pMouse = 0;
			pGm->gd.UnSet(p);
			++nGrow;
			pGm->lvl.nScore += 100 * (pGm->nCombo + 1);

			if (pGm->m_comboClock > 0)
			{
				pGm->nCombo++;
			}

			if(bInvisible)
			{
				//++pGm->nCombo;
				//if(pGm->nCombo > 7)
				//	pGm->nCombo = 7;
			}
		}

		lsPos.push_front(p);
		++pGm->gd.mtx.at(p).nSnakeLayers;
	    
		if(nGrow == 0)
		{
			--pGm->gd.mtx.at(lsPos.back()).nSnakeLayers;
			pGm->gd.UnSet(lsPos.back());
			lsPos.pop_back();
		}
		else
			--nGrow;

		Draw();

		return false;
	}

	return false;
}

std::string Snake::getSnakeChar(std::list<Point>::iterator b, std::list<Point>::iterator f)
{
	switch(f->x-b->x)
	{
	case 0:
		switch(f->y-b->y)
		{
		case 1:
			return "D";
			break;
		case -1:
			return "U";
			break;
		default:
			return f->y-b->y<-1?"D":"U";
			break;
		}
		break;
	case 1:
		return "R";
		break;
	case -1:
		return "L";
		break;
	default:
		return f->x-b->x<-1?"R":"L";
		break;
	}
}
	
void Snake::Draw()
{
	std::string sPrefix = "";

 	if(bRed)
		sPrefix = "RED_";

	int segIdx = lsPos.size();

	for(std::list<Point>::iterator itr = lsPos.begin(), etr = lsPos.end(); itr != etr; ++itr)
	{
		bool deadHere = false;    
		if(pGm->gd.mtx.at(*itr).nSnakeLayers == 1)
		{
			if(!bInvisible || bRed || pGm->lvl.nLives == 0)
			{

				std::string word("");
				//--means to the head, ++ means to tail

				//go towards tail
				std::list<Point>::iterator b = ++itr;
				itr--;
				if(b == etr)
					word+="N";
				else
					word+=getSnakeChar(itr,b);


				//check if head
				if(itr == lsPos.begin())
					word+="N";
				else
				{
					std::list<Point>::iterator f = --itr;
					itr++;
					word+=getSnakeChar(itr,f);
				}



				if (pGm->lvl.nLives == 0)
				{
					//int skelRand = rand();

					//int normalizedAnimCounter = std::max(m_deathAnimCounter - PRE_DEATH_ANIM_PAUSE, 0);

					//int segsToSkip = normalizedAnimCounter/DEATH_STEP_LENGTH;
					//if ( normalizedAnimCounter > (lsPos.size() - 1) * DEATH_STEP_LENGTH
					//	&&
					//	normalizedAnimCounter < (lsPos.size() - 1) * DEATH_STEP_LENGTH + PRE_SKULL_PAUSE )
					//{
					//	segsToSkip = lsPos.size() - 1;
					//}

					if (segIdx > nSkipSegments())
					{
						pGm->gd.mtx.at(*itr).img = pGm->pr["SKELESNAKE"+word];
						pGm->gd.mtx.at(*itr).bDraw = true;
					} 
					else
					{
						deadHere = true;
						pGm->gd.mtx.at(*itr).nSnakeLayers = 1;
					}
				} 
				else 
				{
					if(sPrefix == "") 
						pGm->gd.mtx.at(*itr).img = mpSnakeImages["SNAKE"+word];
					else
						pGm->gd.mtx.at(*itr).img = pGm->pr[sPrefix+"SNAKE"+word];
					
					pGm->gd.mtx.at(*itr).bDraw = true;
				}

				/*
				if(pGm->gd.mtx.at(*itr).img.vImage.empty())
				{
				pGm->gd.mtx.at(*itr).img = pGm->pr("snake_body");
				pGm->gd.mtx.at(*itr).bDraw = true;
				}*/
			}
			else
			{
				pGm->gd.UnSet(*itr);
			}
		}

		if(pGm->gd.mtx.at(*itr).nSnakeLayers > 1)
		{
			//pGm->gd.mtx.at(*itr).img = pGm->pr["snake_over"];
			//pGm->gd.mtx.at(*itr).bDraw = true;
			if (segIdx > nSkipSegments())
			{
				pGm->gd.Set(*itr, pGm->pr["snake_over"]);
			} 
			else 
			{
				//figure out if this crossover is still being drawn further up the chain. otherwise erase it.
				int segIdx2 = segIdx;
				bool drawAnyway = false;
				for (std::list<Point>::iterator checkAheadItr(itr); checkAheadItr != lsPos.end(); checkAheadItr++)
				{
					if (*checkAheadItr == *itr && segIdx2 != segIdx && segIdx2 <= nSkipSegments())
					{
						drawAnyway = true;
						break;
					}
					segIdx2 --;
				}

				if (drawAnyway)
				{
					pGm->gd.mtx.at(*itr).UnSetImage();
					pGm->gd.mtx.at(*itr).bDraw = false;
				}

			}
		}


		if (deadHere)
		{
			pGm->gd.mtx.at(*itr).UnSetImage();
			pGm->gd.mtx.at(*itr).bDraw = false;
		}

		segIdx--;
	}

	if (m_deathAnimState == STATE_POST_DEATH_PAUSE)
	{
		Gui::Point headPt = *(lsPos.begin());
		if (m_deathAnimCounter == 0)
		{
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					Gui::Point areaPt(headPt.x + i, headPt.y + j);
					Rectangle r(pGm->gd.szTotal);
					if(InsideRectangle(r, areaPt))
						pGm->gd.mtx.at(areaPt).UnSetImage();
					//pGm->gd.mtx.at(areaPt).bDraw = false;
				}
			}
		}
		//headPt.x -=1;
		//headPt.y -=1;
		if (m_deathAnimCounter <= 6)
		{
			if (m_deathAnimCounter <= 1)
			{
				pGm->gd.mtx.at(headPt).img = pGm->pr["explosion1"];
				pGm->gd.mtx.at(headPt).bCenter = true;
			}
			else
			{
				pGm->gd.mtx.at(headPt).img = pGm->pr["explosion2"];
				pGm->gd.mtx.at(headPt).bCenter = true;
			}
			pGm->gd.mtx.at(headPt).bDraw = true;
		}
		else
		{
			pGm->gd.mtx.at(headPt).UnSetImage();
			pGm->gd.mtx.at(headPt).bDraw = false;
		}
	}
}

void DirToPic(Point p, Mouse* pMs)
{
    if(p.x == 1)
        pMs->sPic = "mouse_right_0";
    else if(p.x == -1)
        pMs->sPic = "mouse_left_0";

    pMs->bPicToggle = !pMs->bPicToggle;

    if(pMs->bPicToggle)
        pMs->sPic[pMs->sPic.size() - 1] = '0';
    else
        pMs->sPic[pMs->sPic.size() - 1] = '1';
}

Point LinDirRand()
{
    if(rand()%2)
        return Point(rand()%2 * 2 - 1, 0);
    else
        return Point(0, rand()%2 * 2 - 1);
}

Point DirRand()
{
    return Point(rand()%3 - 1, rand()%3 - 1);
}

void Mouse::Move()
{
#ifdef OLD_MOUSE_MOVEMENT
	if(double(rand())/RAND_MAX < dProb)
    {
        Point pMv = LinDirRand();
        
        Point pNew = p + pMv;//Point(rand()%3 - 1, rand()%3 - 1);
        gd.Teleport(pNew);

        if(gd.mtx.at(pNew).bSolid || gd.mtx.at(pNew).nSnakeLayers > 0 || gd.mtx.at(pNew).pMouse)
            return;

        gd.UnSet(p);
        gd.mtx.at(p).pMouse = 0;
        p = pNew;
        DirToPic(pMv, this);
        gd.Set(p, pr[sPic]);
        gd.mtx.at(p).pMouse = this;
    }
#else
	
	if(double(rand())/RAND_MAX < dProb)
    {
		fFloatVel.x += (float(rand())/RAND_MAX - .5)*fRandomWalk;
		fFloatVel.y += (float(rand())/RAND_MAX - .5)*fRandomWalk;

		if(fFloatVel.Length() > fMaxSpeed)
			fFloatVel.Normalize(fMaxSpeed);

	}

	fPoint fOld = fFloatPos;
	fFloatPos += fFloatVel;
	Point pNew = fFloatPos.ToPnt();
    gd.Teleport(pNew);
	
	if(pNew == p)
		return;

	if(gd.mtx.at(pNew).bSolid || gd.mtx.at(pNew).nSnakeLayers > 0 || gd.mtx.at(pNew).pMouse)
	{
		fFloatPos = p;
		fFloatVel = fPoint(0,0);
	}
	else
	{
        gd.UnSet(p);
        gd.mtx.at(p).pMouse = 0;
        DirToPic(pNew - p, this);
		p = pNew;
        gd.Set(p, pr[sPic]);
        gd.mtx.at(p).pMouse = this;
	}
#endif
}

float Distance(Point p1, Point p2, Size szBox, int nWallCode)
{
    if(nWallCode == 0 || nWallCode == 1)
    {
        if(p1.x < p2.x)
            std::swap(p1, p2);
        if(p1.x - p2.x > szBox.x / 2)
            p2.x += szBox.x;
    }

    if(nWallCode == 0 || nWallCode == 2)
    {
        if(p1.y < p2.y)
            std::swap(p1, p2);
        if(p1.y - p2.y > szBox.y / 2)
            p2.y += szBox.y;
    }

    return (fPoint(p1) - fPoint(p2)).Length();
}




void Mouse::Move(Point pSnake)
{
#ifdef OLD_MOUSE_MOVEMENT
	Point pNew = p;
    
    float f = Distance(p, pSnake, gd.szTotal, nWallCode);
    if(f < nDist)
    {
        Point pMv;
        for(int i = 0; i < 30; ++i)
        {
            pMv = LinDirRand();
            
            Point pN = p + pMv;
            gd.Teleport(pN);
            if(gd.mtx.at(pN).bSolid || gd.mtx.at(pN).nSnakeLayers > 0 || gd.mtx.at(pN).pMouse)
                continue;
            if(Distance(pN, pSnake, gd.szTotal, nWallCode) > f)
            {
                pNew = pN;
                goto here;
            }
        }
here:;
        if(pNew == p)
            return;

        gd.UnSet(p);
        gd.mtx.at(p).pMouse = 0;
        p = pNew;
        DirToPic(pMv, this);
        gd.Set(p, pr[sPic]);
        gd.mtx.at(p).pMouse = this;
    }
    else
        Move();
#else
    Point pNew = p;
    
    float f = Distance(p, pSnake, gd.szTotal, nWallCode);
    if(f < nDist)
    {
        fPoint fTarVel;

		Point pMv;
        for(int i = 0; i < 30; ++i)
        {
            pMv = DirRand();
			fTarVel = pMv;
            
            Point pN = p + pMv;
            gd.Teleport(pN);
            if(gd.mtx.at(pN).bSolid || gd.mtx.at(pN).nSnakeLayers > 0 || gd.mtx.at(pN).pMouse)
                continue;
            if(Distance(pN, pSnake, gd.szTotal, nWallCode) > f)
            {
                pNew = pN;
                goto here;
            }
        }
here:;
        if(pNew == p)
            return;

		fFloatPos = pNew;
		fFloatVel = fTarVel;
		fFloatVel.Normalize(fMaxSpeed);

        gd.UnSet(p);
        gd.mtx.at(p).pMouse = 0;
        p = pNew;
        DirToPic(pMv, this);
        gd.Set(p, pr[sPic]);
        gd.mtx.at(p).pMouse = this;
    }
    else
        Move();
#endif

}



void Grid::Set(Point p, Index img, bool bSolid/* = false*/)
{
    mtx.at(p).SetImage(img, bSolid);
}

void Grid::UnSet(Point p)
{
    mtx.at(p).UnSetImage();
}

ActualGame::ActualGame(MyCamera cc_, Size szElement_, Background bkgr_, MyPreloader& pr_, SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo lvl_, SP<Event> pLoseEvent_, SP<GameController>* ppCurrControl_, SP<HighScoreController> pHigh_, LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, InfoPack ipck_, bool bPreProgrammed_)
:gd(cc, Size(), szElement_), bkgr(bkgr_), pr(pr_), cc(cc_), pFont(pFont_), pFontCut(pFontCut_), lvl(lvl_), pLoseEvent(pLoseEvent_), ppCurrControl(ppCurrControl_), ipck(ipck_), bAtContinueScreen(false),
szElement(szElement_), pHigh(pHigh_), lpu(lpu_), pSel(pSel_), pSndMng(pSndMng_), ts(TS_PLAYING), nCombo(0), m_comboClock(0), bPreProgrammed(bPreProgrammed_), nKeyPos(0), pDialogBox(0),
tFlashStart(30)
{
	InitializeFromLevel();


	pSn = new Snake(this, ipck.snDt, ipck.bPlayer1 ? Color(0, 255, 0) : Color(0, 0, 255));

	if(lvl.lvCurr.bInv)
        pSn->bInvisible = true;

    pSn->Draw();

	if(!bPreProgrammed)
	{
		SP<EventAnimationController> pInit3 = new EventAnimationController(cc, pSndMng, cc.GetBox(), Color(0,0,0),
			NewCpSwitchEvent< SP<GameController>, SP<GameController> >(pDialogBox, 0), false, true);

		SP<AnimationOnce> pAn3 = new AnimationOnce(pr("readygo"), cc.fromF(fPoint(.5,.5)), (100/nFrameRate)*1, true,
			NewTerminatorEvent(pInit3.GetRawPointer()));

		pInit3->NewAnimation(pAn3);

		pDialogBox = pInit3;
	}
}

void ActualGame::InitializeFromLevel()
{
	Size sz = lvl.lvCurr.mtxObjects.GetSize();
	Point p;

	szTotal = sz;

	gd = Grid(cc, szTotal, szElement);
	
	Size szDr = Size(szTotal.x * szElement.x, szTotal.y * szElement.y);

	gd.cc.BX_Translate(Point((gd.cc.GetBox().x - szDr.x)/2, (gd.cc.GetBox().y - szDr.y)/2));

	for(p.x = 0; p.x < sz.x; ++p.x)
	for(p.y = 0; p.y < sz.y; ++p.y)
	{
		int nCd = lvl.lvCurr.mtxObjects.at(p);
		if(nCd == 0)
			continue;
		
		if(nCd == -1)
			gd.Set(p, pr["wall"], true);
		else if(nCd == -2)
		{
			if(!lvl.lvCurr.bRndBoulders)
				gd.Set(p, pr["boulder"], true);
		}
		else if(!lvl.lvCurr.bRndMice)
		{
			MiceDescr md = lvl.lvCurr.vMice.at(nCd - 1);
			vMice.push_back(new Mouse(p, md.dRate, gd, pr, md.nRange, md.nWallCode, md.fMaxSpeed, md.fRandomWalk));
		}
	}

	if(lvl.lvCurr.bRndBoulders)
	{
		for(int i = 0; i < lvl.lvCurr.nRndWall; ++i)
		{
			Point p;
			p.x = rand()%szTotal.x;
			p.y = rand()%szTotal.y;

			if(!gd.mtx[p].bDraw)
				gd.Set(p, pr["boulder"], true);
		}
	}

	if(lvl.lvCurr.bRndMice)
	{
		for(size_t j = 0; j < lvl.lvCurr.vMice.size(); ++j)
		{
			MiceDescr md = lvl.lvCurr.vMice[j];
			for(int i = 0; i < lvl.lvCurr.vMice[j].nRndNum; ++i)
			{
				Point p;
				p.x = rand()%szTotal.x;
				p.y = rand()%szTotal.y;

				if(!gd.mtx[p].bDraw)
					vMice.push_back(new Mouse(p, md.dRate, gd, pr, md.nRange, md.nWallCode, md.fMaxSpeed, md.fRandomWalk));
			}
		}
	}
}


/*virtual*/ void ActualGame::OnKey(GuiKeyType nCode, bool bUp)
{
    if(bUp)
        return;

    if(nCode == GUI_ESCAPE)
    {
        Lose(false);
        return;
    }

	if(pDialogBox != 0)
	{
		if(bAtContinueScreen)
		{
			lvl.LoadLevel(lvl.nLevel);

			lvl.nScore = 0;
			lvl.nLives = lvl.nInitialLives;

			SP<NewGameEvent> pNext = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szElement,
				bkgr, pr, pFont, pFontCut, lvl, pHigh, lpu, pSel, pSndMng, ipck);
			pNext->Trigger();
			return;
		}

		return;
	}

	if(nCode == '/')
		nCode = GuiKeyType(' ');

	if(ipck.gd == GD_MOBILE)
	{
		if(nCode == 'i')
			nCode = GUI_UP;
		if(nCode == 'j')
			nCode = GUI_LEFT;
		if(nCode == 'k')
			nCode = GUI_DOWN;
		if(nCode == 'l')
			nCode = GUI_RIGHT;
		if(nCode == GUI_RETURN)
			nCode = GuiKeyType(' ');
	}
	else if(!ipck.bPlayer1)
	{
		if(nCode == GUI_UP || nCode == GUI_DOWN || nCode == GUI_LEFT || nCode == GUI_RIGHT
			|| nCode == GuiKeyType(' ') || nCode == GuiKeyType('2'))
			nCode = GuiKeyType('i');
		else
		{
			if(nCode == 'i')
				nCode = GUI_UP;
			if(nCode == 'j')
				nCode = GUI_LEFT;
			if(nCode == 'k')
				nCode = GUI_DOWN;
			if(nCode == 'l')
				nCode = GUI_RIGHT;
			if(nCode == GUI_RETURN)
				nCode = GuiKeyType(' ');
			if(nCode == GuiKeyType('1'))
				nCode = GuiKeyType('2');
		}
	}

	bool bDir = (nCode == GUI_UP || nCode == GUI_DOWN || nCode == GUI_LEFT || nCode == GUI_RIGHT);

	Point pNewDir = pSn->pDir;
	Point prevDir = pSn->pDir;

	if(nCode == GUI_UP)
	{
        pNewDir = Point(0,-1);
	}
    else if(nCode == GUI_DOWN)
	{
        pNewDir = Point(0,1);
	}
    else if(nCode == GUI_LEFT)
	{
        pNewDir = Point(-1,0);
	}
    else if(nCode == GUI_RIGHT)
	{
        pNewDir = Point(1,0);
	}
    else if(char(nCode) == ' ')
    {
		if(lvl.lvCurr.bInv)
		{
			// Alex: play 'cannot turn invisible' sound

			return;
		}
        
        if(pSn->bInvisible)
        {
            /*
			if(lvl.nScore < 10)
                lvl.nScore = 0;
            else
                lvl.nScore -= 10;
				*/
			//nCombo = 0;
        }
        
        pSn->bInvisible = !pSn->bInvisible;
		//
		/*if (pSn->bInvisible)
		{
			pSn->m_invisibleFlickerTime = FLICKER_DURATION;
		}*/

		if (ts == TS_PLAYING) //don't play invisibility sounds on win!
		{
        if(pSn->bInvisible)
            pSndMng->PlaySound(pr.GetSnd("invisible"), 3);
        else
            pSndMng->PlaySound(pr.GetSnd("visible"), 3);
		}
    }
    else if(char(nCode) == '\\')
    {
		plr.StopMusic();
		pSndMng->PlaySound(pr.GetSnd("win_level"));

		ts = TS_WIN;
		tTerminal = Timer(WIN_DELAY);
        //Win(); //TODO: currently win waits another tick so the last mouse actually gets eaten.
		//If there are lots of mice left, then it just doesn't work.
        return;
    }
    else if(char(nCode) == ']')
    {
		++pSn->nGrow;
	}
	else if(char(nCode) == '2')
	{
		if(ipck.gd == GD_ARCADE)
		{
			(**ipck.ppTwoPlayerGameEvent).pLoseEvent = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szElement,
					bkgr, pr, pFont, pFontCut, lvl, pHigh, lpu, pSel, pSndMng, ipck);

			SP<EventAnimationController> pChallenger = new EventAnimationController(cc, pSndMng, cc.GetBox(), Color(0,0,0),
				*ipck.ppTwoPlayerGameEvent, false, true);

			SP<AnimationTimer> pAn3 = new AnimationTimer(pr("challenger"), cc.fromF(fPoint(.5,.5)), (500/nFrameRate)*1, true,
				NewTerminatorEvent(pChallenger.GetRawPointer()), 1000/nFrameRate * 2);

			pChallenger->NewAnimation(pAn3);

			pDialogBox = pChallenger;
		}
	}

    if(bDir && Point(-pNewDir.x, -pNewDir.y) != pSn->pDir )
    {
        pSn->pNextDir = pNewDir;

		//Play appropriate turn sound.
		/*
		if (pNewDir != pSn->pDir)
		{
		if (pSn->pNextDir.x == -1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn01"));
		} 
		else if (pSn->pNextDir.y == 1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn02"));
		} 
		else if (pSn->pNextDir.x == 1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn03"));
		} 
		else if (pSn->pNextDir.y == -1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn04"));
		} 
		}*/

        if(bDir)
		{
            pSn->bBoomLock = false;
		}
    }

}

/*virtual*/ void ActualGame::OnClick(Point p)
{
	//OnKey(GuiKeyType(' '), false);
}



/*virtual*/ void ActualGame::Update()
{
    if(!bPreProgrammed)
	{
		bIgnoreSpaceReturn = false;
		
		bkgr.Draw(gd.cc);

		Size sz = gd.cc.fromR(pFont->szSymbol);

		tFlashStart.Tick();
		
		if(ipck.gd == GD_ARCADE)
		{
		
			gd.cc.Push();
			
			gd.cc.Translate( Point(0, -sz.y) );

			std::string s1 = "  PLAYER ONE";

			if(!ipck.bPlayer1)
			{
				if((tFlashStart.nTimer%tFlashStart.nPeriod) < tFlashStart.nPeriod/2)
					s1 = "  PRESS START";
				else
					s1 = "";
			}
			
			pFont->DrawWord(s1, gd.cc.toR(Point()));

			gd.cc.Pop();
			
			gd.cc.Push();
			
			gd.cc.Translate(Point(gd.cc.GetBox().x - pFont->GetSize("PRESS START  ").x, -sz.y) );

			std::string s2 = " PLAYER TWO";

			if(ipck.bPlayer1)
			{
				if((tFlashStart.nTimer%tFlashStart.nPeriod) < tFlashStart.nPeriod/2)
					s2 = "  PRESS START";
				else
					s2 = "";
			}

			pFont->DrawColorWord(s2, gd.cc.toR(Point()), Color(0, 0, 255));

			gd.cc.Pop();
		}

		gd.cc.Push();

		gd.cc.BX_Translate(Point(-1,-1));
		gd.cc.DrawRectangle(gd.cc.GetBox(), Color(100,100,100));

		gd.cc.Pop();

		gd.cc.DrawRectangle(gd.cc.GetBox(), Color(0,0,0));

		std::string str;
		str += "SCORE " + pFont->GetNumber(lvl.nScore, 6);
		str += "   ";
		str += "LEVEL " + pFont->GetNumber(lvl.nLevel, 2);
		str += "   ";
		str += "LIVES " + pFont->GetNumber(lvl.nLives, 1);
		str += "   ";
		str += "COMBO " + pFont->GetNumber(nCombo, 1);

		gd.cc.Push();

		gd.cc.Translate(fPoint(.5, 0));
		gd.cc.Translate(Point(0, -sz.y/2));

		if(ipck.gd == GD_MOBILE)
			pFont->DrawWord(str, gd.cc.toR(Point()), true);
		else
			pFont->DrawColorWord(str, gd.cc.toR(Point()), Color(255, 255, 255), true);
	    
		gd.cc.Pop();
	}

	if(bPreProgrammed && vKeys.size())
	{
		OnKey(vKeys[nKeyPos], false);
		if(++nKeyPos >= vKeys.size())
			nKeyPos = 0;
	}

	if(pDialogBox != 0)
	{
		SP<ActualGame> pGm(this);
		
		gd.Draw();
		pDialogBox->Update();
		gd.cc.pGr->RefreshAll();
		return;
	}


	//TODO:
	//Move drawing of snake out of "move" function
	//and call it in a more sensible place.  Only then will the below be unnecessary.
	if (ts ==  TS_LOSE)
	{
		pSn->stepDeathAnimation();
		pSn->Draw();
	}

	if(ts == TS_PLAYING)
	{
		if(!bPreProgrammed)
			plr.SwitchTheme(BG_BACKGROUND);

		for(unsigned i = 0; i < vMice.size(); ++i)
		{
			if(vMice[i]->bDead)
				continue;
			if(pSn->bInvisible)
				vMice[i]->Move();
			else
				vMice[i]->Move(pSn->lsPos.front());
		}
		

		pSn->m_invisibleFlickerTime = std::max(0, pSn->m_invisibleFlickerTime - 1);
		if(pSn->Move())
		{
			ts = TS_LOSE;
			tTerminal = Timer(LOSE_DELAY);
		}
	    
		if(gd.Draw() && !bPreProgrammed)
		{
			plr.StopMusic();
			pSndMng->PlaySound(pr.GetSnd("win_level"));

			ts = TS_WIN;
			tTerminal = Timer(WIN_DELAY);
		}

		m_comboClock = std::max(0, m_comboClock - 1);
		if (m_comboClock == 0)
		{
			nCombo = 0;
		}
	}
	else 
	{
		gd.Draw();
		
		if(tTerminal.Tick())
		{
			if(ts == TS_WIN)
			{
				Win();
				return;
			}
			else if (ts == TS_LOSE && pSn->deathAnimationFinished())
			{
				if(ipck.gd != GD_ARCADE)
				{
					Lose();
					return;
				}
				
				if(!bPreProgrammed)
				{
					SP<EventAnimationController> pContinue = new EventAnimationController(cc, pSndMng, cc.GetBox(), Color(0,0,0),
						new LoseGameEvent(this), false, true);

					Size sz1 = cc.pGr->GetImage(pr("continue").GetImage())->GetSize();
					Size sz2 = cc.pGr->GetImage(pr("countdown").GetImage())->GetSize();

					sz1 = cc.fromR(sz1);
					sz2 = cc.fromR(sz2);

					Point p1 = cc.fromF(fPoint(.5,.5));
					Point p2 = cc.fromF(fPoint(.5,.5));

					p1.y -= sz1.y/2 + 2;
					p2.y += sz2.y/2 + 2;
					
					SP<StaticImage> pContImg = new StaticImage(p1, pr("continue").GetImage(), true);
					
					SP<AnimationOnce> pCountdown = new AnimationOnce(pr("countdown"), p2, (100/nFrameRate)*1, true,
						NewTerminatorEvent(pContinue.GetRawPointer()));

					pContinue->NewAnimation(pContImg);
					pContinue->NewAnimation(pCountdown);

					pDialogBox = pContinue;
					bAtContinueScreen = true;
				}
			}
		}
	}

    
    if(!bPreProgrammed)
		gd.cc.pGr->RefreshAll();
}

int ActualGame::getComboTimeBonus()
{
	return 200;
}

void ActualGame::Lose(bool bActual/* = true*/)
{
	if(bPreProgrammed)
		return;

	bIgnoreSpaceReturn = true;

	
	//if (mLoseDelayCount == 0 || !bActual)
	//{

	plr.StopMusic();
	if(!bActual)
	{
		Trigger(pLoseEvent);
		return;
	} 
	else
	{	
		pSndMng->PlaySound(pr.GetSnd("gameover"));
	}

	SP<EventAnimationController> pLose = new EventAnimationController(cc, 0, bkgr.rBound, Color(0,0,0),
		pLoseEvent, true);

	SP<AnimationOnce> pLoseAn = new AnimationOnce(pr("over"), cc.fromF(fPoint(.5, .5)), (300/nFrameRate)*3, true,
		NewTerminatorEvent(pLose.GetRawPointer()));

	pLose->NewAnimation(pLoseAn);

	*ppCurrControl = pLose;
	/*
	}
	else
	{
		gd.Draw();
		if (mLoseDelayCount  == LOSE_DELAY)
		{
			gd.cc.pGr->RefreshAll();
		}
		//gd.cc.pGr->RefreshAll();
		mLoseDelayCount--;
	}
	*/
}
/*
void ActualGame::Win()
{
    pSndMng->PlaySound(pr.GetSnd("win_level"));

    if(pHigh->nScore < lvl.nScore)
        pHigh->nScore = lvl.nScore;

    if(!lvl.LoadLevel(lvl.nLevel + 1))
    {
        SP<Event> pNext = pLoseEvent;

        if(lpu.i == 0 || lpu.i == 1)
        {
            SP<NewGameEvent> pNewGame = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szTotal, szElement,
                bkgr, pr, pFont, lvl, pHigh, lpu, pSel, pSndMng);

            if(lpu.i == 0)
                pNewGame->lvl = pSel->lvlM;
            else
                pNewGame->lvl = pSel->lvlH;

            ++pNewGame->lpu.i;
            
            if(!pNewGame->lvl.LoadLevel(1))
                throw SimpleException("ActualGame","Win","problem loading levels");

            pNewGame->lvl.nLives = lvl.nLives + 1;
            pNewGame->lvl.nScore = lvl.nScore + 1000;
            
            pNext = pNewGame;
        }
        
        SP<EventAnimationController> pWin = new EventAnimationController(cc, 0, bkgr.rBound, Color(0,0,0),
            pNext, true);
        
        SP<AnimationTimer> pWinAn = new AnimationTimer(pr("win"), cc.fromF(fPoint(.5, .5)), (100/nFrameRate)*1, true,
            NewTerminatorEvent(pWin.GetRawPointer()), (100/nFrameRate)*30);

        pWin->NewAnimation(pWinAn);

        *ppCurrControl = pWin;

        return;
    }

    lvl.nLength = pSn->lsPos.size() + pSn->nGrow;
    lpu.Update(lvl.nLevel);

    SP<NewGameEvent> pNewGame = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szTotal, szElement,
        bkgr, pr, pFont, l
		vl, pHigh, lpu, pSel, pSndMng);
    SP<CutSceneController> pCut = new CutSceneController(pFont, cc, pNewGame, bkgr, 0);
    *ppCurrControl = pCut;

    
    //*ppCurrControl = new ActualGame(cc, szTotal, szElement, bkgr, pr, pFont, lvl, pLoseEvent, ppCurrControl, pHigh, lpu, pSel, pSndMng);
}
*/


void ActualGame::Win()
{
	bIgnoreSpaceReturn = true;
	
	/*if ((mWinDelayCount > 0)) //delay move on to next level
	{
		if (mWinDelayCount == WIN_DELAY)
		{
			plr.StopMusic();
			pSndMng->PlaySound(pr.GetSnd("win_level"));
			//Move the snake again, so the mouse will disappear
			//pSn->Move();
			gd.cc.pGr->RefreshAll();
		}
		
		
		mWinDelayCount --;
	} 
	else // mWinDelayCount == 0;
	{*/

	if(bPreProgrammed)
		return;

	if(pHigh->nScore < lvl.nScore)
		pHigh->nScore = lvl.nScore;
	
	SP<Event> pNext;
	//mWinDelayCount = WIN_DELAY;
	int nCutSceneNum = lpu.i * 5 + lvl.nLevel;
	
	bool bFinalScreen = false;
	int nCutSceneNumFinal = 0;


    if(!lvl.LoadLevel(lvl.nLevel + 1))
    {
        bFinalScreen = true;
		
		pNext = pLoseEvent;

        if(lpu.i == 0 || lpu.i == 1)
        {
            SP<NewGameEvent> pNewGame = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szElement,
                bkgr, pr, pFont, pFontCut, lvl, pHigh, lpu, pSel, pSndMng, ipck);

            if(lpu.i == 0)
			{
                pNewGame->lvl = pSel->lvlM;
				nCutSceneNumFinal = 16;
			}
            else
			{
                pNewGame->lvl = pSel->lvlH;
				nCutSceneNumFinal = 17;
			}

            ++pNewGame->lpu.i;
            
            if(!pNewGame->lvl.LoadLevel(1))
                throw SimpleException("ActualGame","Win","problem loading levels");

            pNewGame->lvl.nLives = lvl.nLives + 1;
            pNewGame->lvl.nScore = lvl.nScore + 1000;
            
            pNext = pNewGame;

            nCutSceneNum = pNewGame->lpu.i * 5 + pNewGame->lvl.nLevel - 1;
			//pSndMng->PlaySound(pr.GetSnd("cutscene_" + S((nCutSceneNum-1)%4 + 1)));
        }
		else
		{
			nCutSceneNumFinal = 18;
		}
    }
    else
    {
		//TODO: not the best place to play this sound...
		
        //lvl.nLength = pSn->lsPos.size() + pSn->nGrow;
        lpu.Update(lvl.nLevel);

        pNext = new NewGameEvent(ppCurrControl, pLoseEvent, cc, szElement,
            bkgr, pr, pFont, pFontCut, lvl, pHigh, lpu, pSel, pSndMng, ipck);

        nCutSceneNum = lpu.i * 5 + lvl.nLevel - 1;

		//pSndMng->PlaySound(pr.GetSnd("cutscene_" + S((nCutSceneNum-1)%4 + 1)));
    }


	SP<CutSceneController> pCut = new CutSceneController(pSndMng, pFontCut, cc, pNext, bkgr, nCutSceneNum, pr);

	if(bFinalScreen)
	{
		SP<CutSceneController> pCutFinal = new CutSceneController(pSndMng, pFontCut, cc,
			NewCpSwitchEvent(*ppCurrControl, pCut),
			bkgr, nCutSceneNumFinal, pr);
		*ppCurrControl = pCutFinal;
	}
	else
	{
		*ppCurrControl = pCut;
	}

    


	//}
}



void SelectElement::Draw(SP<FontWriter> pFont, MyCamera cc)
{
    Color cCurr = bSelected ? cSel : cDef;

	if(cCurr == Color(0,0,0))
		return;

    cc.Push();

    cc.Translate(p);

    cc.Push();

    cc.SetBox(Size(0,0));
    cc.BX_Translate(Point(-sz.x/2, -sz.y/2));
    
    cc.DrawRectangle(cc.GetBox(), cCurr, false);

	r = Rectangle(cc.toR(Point()), cc.toR(cc.GetBox()));

    cc.BX_Translate(Point(1, 1));
    cc.DrawRectangle(cc.GetBox(), Color(), false);

    cc.Pop();

    if(!bSimple)
		pFont->DrawColorWord(sText, cc.toR(Point()), cCurr, true);
	else
		pFont->DrawWord(sText, cc.toR(Point()), true);

    cc.Pop();
}

SelectGrid::SelectGrid(Size sz_, Point pPos_, Size szSpacing_, SP<FontWriter> pFont_, MyCamera cc_)
:sz(sz_), pPos(pPos_), szSpacing(szSpacing_), pFont(pFont_), mtxEl(sz_), cc(cc_)
{}

void SelectGrid::Space()
{
    Point p;
    for(p.y = 0; p.y < sz.y; ++p.y)
    {
        int nSum = 0;
        for(p.x = 0; p.x < sz.x; ++p.x)
        {
            nSum += mtxEl.at(p).sz.x;
            if(p.x != p.x - 1)
                nSum += szSpacing.x;
        }

        int nInX = pPos.x - nSum/2;
        
        for(p.x = 0; p.x < sz.x; ++p.x)
        {
            mtxEl.at(p).p.x = nInX + mtxEl.at(p).sz.x/2;
            nInX += mtxEl.at(p).sz.x + szSpacing.x;
        }
    }

    p.x = 0;

    int nSum = 0;
    for(p.y = 0; p.y < sz.y; ++p.y)
    {
        nSum += mtxEl.at(p).sz.y;
        if(p.y != p.y - 1)
            nSum += szSpacing.y;
    }

    int nInY = pPos.y - nSum/2;
    
    for(p.y = 0; p.y < sz.y; ++p.y)
    {
        for(p.x = 0; p.x < sz.x; ++p.x)
            mtxEl.at(p).p.y = nInY + mtxEl.at(p).sz.y/2;
        p.x = 0;
        nInY += mtxEl.at(p).sz.y + szSpacing.y;
    }
}

void SelectGrid::Draw()
{
    Point p;
    for(p.y = 0; p.y < sz.y; ++p.y)
    for(p.x = 0; p.x < sz.x; ++p.x)
        mtxEl.at(p).Draw(pFont, cc);
}

LevelSelectController::LevelSelectController(SP<Event> pExitEvent_, SP<NewGameEvent> pNewGameEvent_,
                                             SP<FontWriter> pFont_, Background bkgr_, FilePath& fp_, LevelProgress& lp_, MyCamera cc_, MenuSounds msnd_, bool bEsc_, GAME_DEVICE gd_)
:pExitEvent(pExitEvent_), pNewGameEvent(pNewGameEvent_), bkgr(bkgr_), pFont(pFont_), fp(fp_), lp(lp_), bEsc(bEsc_),
gr(Size(6,3), cc_.fromF(fPoint(.5, .5)), Size(10, 10), pFont_, cc_), pPos(1,0), cc(cc_), msnd(msnd_), gd(gd_)
{
    std::string sLvlProgr = "level_progress.txt";
    fp.Parse(sLvlProgr);
	
	SP<InStreamHandler> pFl = fp.ReadFile(sLvlProgr);
	std::istream& ifs = pFl->GetStream();
    
    ifs >> lp;

    Point p;
    for(p.y = 0; p.y < gr.sz.y; ++p.y)
    for(p.x = 0; p.x < gr.sz.x; ++p.x)
    {
        gr.mtxEl.at(p).sz = Size(25, 25);
		gr.mtxEl.at(p).bSimple = (gd == GD_MOBILE);
        
        if(p.x == 0)
        {
            if(p.y == 0)
                gr.mtxEl.at(p).sText = "E";
            else if(p.y == 1)
                gr.mtxEl.at(p).sText = "M";
            else
                gr.mtxEl.at(p).sText = "H";
        }
        else
            gr.mtxEl.at(p).sText = pFont->GetNumber(p.x);
    }

    gr.Space();

    lvlE.Read(cc, fp, "E_");
    lvlM.Read(cc, fp, "M_");
    lvlH.Read(cc, fp, "H_");
    
    if(!lvlE.LoadLevel(1) || !lvlM.LoadLevel(1) || !lvlH.LoadLevel(1))
        throw SimpleException("LevelSelectController", "<constructor>", "Problem loading levels");

}

LevelSelectController::~LevelSelectController()
{
    std::string sLvlProgr = "level_progress.txt";
    fp.Parse(sLvlProgr);
	
	SP<OutStreamHandler> pFl = fp.WriteFile(sLvlProgr);
	std::ostream& ofs = pFl->GetStream();
    
	ofs << lp;
}


/*virtual*/ void LevelSelectController::Update()
{
    Point p;
    for(p.y = 0; p.y < gr.sz.y; ++p.y)
    for(p.x = 0; p.x < gr.sz.x; ++p.x)
    {
        gr.mtxEl.at(p).cDef = Color(100, 100, 100);
        gr.mtxEl.at(p).cSel = Color(255, 255, 255);
        
        if(p.x == 0)
            gr.mtxEl.at(p).cDef = Color(0, 255, 0);

        if(int(lp.v.at(p.y)) < p.x)
            gr.mtxEl.at(p).cDef = Color(0, 0, 0);
    }


    for(p.y = 0; p.y < gr.sz.y; ++p.y)
    for(p.x = 0; p.x < gr.sz.x; ++p.x)
        gr.mtxEl.at(p).bSelected = (p == pPos);
    
    
    bkgr.Draw(pNewGameEvent->cc);
    gr.Draw();
    pNewGameEvent->cc.pGr->RefreshAll();

}

/*virtual*/ void LevelSelectController::OnClick(Point pClk)
{
	Point p;
    for(p.y = 0; p.y < gr.sz.y; ++p.y)
    for(p.x = 1; p.x <= lp.v.at(p.y); ++p.x)
	{
		if(InsideRectangle(gr.mtxEl[p].r, pClk))
		{ 
			pPos = p;
			Trigger(msnd.UpSound);
			OnKey(GUI_RETURN, false);

			return;
		}
	}
}

/*virtual*/ void LevelSelectController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(bUp)
        return;

    Point pNewPos = pPos;
    
    if(nCode == GUI_UP)
	{
        --pNewPos.y;
		Trigger(msnd.UpSound);
	}
    else if(nCode == GUI_DOWN)
	{
        ++pNewPos.y;
		Trigger(msnd.DownSound);
	}
    else if(nCode == GUI_LEFT)
	{
        --pNewPos.x;
		Trigger(msnd.LeftSound);
	}
    else if(nCode == GUI_RIGHT)
	{
        ++pNewPos.x;
		Trigger(msnd.RightSound);
	}
    else if(nCode == GUI_RETURN || nCode == GUI_F1)
    {
		Trigger(msnd.InSound);
        LevelInfo* pLvl = 0;
        if(pPos.y == 0)
            pLvl = &lvlE;
        else if(pPos.y == 1)
            pLvl = &lvlM;
        else
            pLvl = &lvlH;

        /*
		unsigned nLength = pLvl->LengthRecursive(pPos.x);

        if(nLength == 0)
            throw SimpleException("LevelSelectController", "OnKey", "Problem loading levels");
			*/

		if(!pLvl->LoadLevel(pPos.x))
			throw SimpleException("LevelSelectController", "OnKey", "Problem loading levels");

        pNewGameEvent->lvl = *pLvl;
        //pNewGameEvent->lvl.nLength = nLength;
        pNewGameEvent->lpu.i = pPos.y;

        Trigger(pNewGameEvent);
    }
    else if(nCode == GUI_ESCAPE)
    {
		Trigger(msnd.OutSound);
        Trigger(pExitEvent);
    }

    if(pNewPos.x >= gr.sz.x)
        pNewPos.x = 1;
    if(pNewPos.x < 1)
        pNewPos.x = gr.sz.x - 1;

    if(pNewPos.y >= gr.sz.y)
        pNewPos.y = 0;
    if(pNewPos.y < 0)
        pNewPos.y = gr.sz.y - 1;

    if(int(lp.v.at(pNewPos.y)) >= pNewPos.x)
        pPos = pNewPos;
}

std::ostream& operator << (std::ostream& ostr, const LevelProgress& pr)
{
    return ostr << pr.v[0] << " " << pr.v[1] << " " << pr.v[2];
}

std::istream& operator >> (std::istream& istr, LevelProgress& pr)
{
    return istr >> pr.v[0] >> pr.v[1] >> pr.v[2];
}

/*virtual*/ void ControlsController::OnKey(GuiKeyType nCode, bool bUp)
{
	SubMenu::OnKey(nCode, bUp);
    if(!bUp)
        Trigger(pTerminate);
}

/*virtual*/ void ControlsController::Update()
{
    bkgr.Draw(cc);

    cc.Push();

    cc.Translate(fPoint(.5, .5));
    
    std::vector<std::string> vText;

    vText.push_back("CONTROLS");
    vText.push_back("");
    vText.push_back("PLAYER 1");
    vText.push_back("Arrow keys to move");
    vText.push_back("Space or ? to turn invisible");
    vText.push_back("");
    vText.push_back("PLAYER 2");
    vText.push_back("WASD to move");
    vText.push_back("Enter or V to turn invisible");

	/*
    vText.push_back("");
    
	vText.push_back("HINT");
    
    if(n == 0)
    {
        vText.push_back("Eat all mice to progress");
        vText.push_back("to the next level");
    }

    if(n == 1)
    {
        vText.push_back("On some levels mice will run away");
        vText.push_back("if you are visible");
    }

    if(n == 2)
    {
        vText.push_back("You don't lose a life if ");
        vText.push_back("you run into yourself when invisible");
    }
	*/

    Size sz = cc.fromR(pFnt->szSymbol);
    int nHeight = vText.size() * sz.y;
    cc.Translate(Point(0, -nHeight/2));

    for(unsigned i = 0; i < vText.size(); ++i)
    {
        pFnt->DrawWord(vText[i], cc.toR(Point()), true, false);
        cc.Translate(Point(0, sz.y));
    }

    cc.Pop();

    cc.pGr->RefreshAll();
}

/*virtual*/ 
void CreditsController::OnKey(GuiKeyType nCode, bool bUp)
{
	SubMenu::OnKey(nCode, bUp);
    if(!bUp)
        Trigger(pTerminate);
}

/*virtual*/ void CreditsController::OnClick(Point p)
{
	Trigger(msnd.OutSound);
	Trigger(pTerminate);
}


/*virtual*/ void CreditsController::Update()
{
    bkgr.Draw(cc);
    
    cc.Push();

    cc.Translate(fPoint(.5, .5));
    
    std::vector<std::string> vText;

    vText.push_back("CREDITS");
    vText.push_back("Anton Bobkov");
    vText.push_back("Peter Lu");
	vText.push_back("Alex Rickett");

    Size sz = cc.fromR(pFnt->szSymbol);
    int nHeight = vText.size() * sz.y;
    cc.Translate(Point(0, -nHeight/2));

    for(unsigned i = 0; i < vText.size(); ++i)
    {
        pFnt->DrawWord(vText[i], cc.toR(Point()), true, false);
        cc.Translate(Point(0, sz.y));
    }

    cc.Pop();

    cc.pGr->RefreshAll();
}

ProgramInfo GetProgramInfo()
{
    ProgramInfo inf;
    
    //inf.szScreenRez = Size(640, 480);
#ifdef CUTE_AND_COMFY
	inf.szScreenRez = Size(854, 480);
	//inf.szScreenRez = Size(960,640);
#else
	inf.szScreenRez = Size(1024, 768);
#endif
    //inf.szScreenRez = Size(1000, 600);
    inf.strTitle = "Snaketrix";
    inf.nFramerate = nFrameRate;
	
	inf.bFullScreen = true;
	inf.bFlexibleResolution = true;
	inf.bMouseCapture = true;
    
    return inf;
}

SP<GlobalController> GetGlobalController(ProgramEngine pe)
{
    return new SnakeGameGlobalController(pe);
}

void AddCycle(int nDir, int nL, std::vector<GuiKeyType>& v)
{
	GuiKeyType kt[] = {GUI_DOWN, GUI_RIGHT, GUI_UP, GUI_LEFT};

	for(int i = 0; i < nL*4; ++i)
	{
		if(i % nL == 0 && i != 0)
			v.push_back(kt[nDir++ % 4]);
		else if(i == (3*nL + 1))
			v.push_back(GuiKeyType(' '));
		else
			v.push_back(GuiKeyType('a'));
		v.push_back(GuiKeyType('a'));
	}
}

void SparseVector(std::vector<GuiKeyType>& v)
{
	std::vector<GuiKeyType> vCp = v;

	v.clear();

	for(size_t i = 0, sz = vCp.size(); i < sz; ++i)
	{
		v.push_back(vCp[i]);
		v.push_back(GuiKeyType('a'));
	}
}

SP<FileManager> GetFileManager();

SnakeGameGlobalController::SnakeGameGlobalController
    (ProgramEngine pe)
    :pGr(pe.pGr), pSndMng(pe.pSndMng), pExitProgram(pe.pExitProgram), gd(GAME_DEVICE_TYPE)
{
    Rectangle sBound = Rectangle(Point(0, 0), pe.szScreenRez);
	unsigned nScale = 1;
    Rectangle rBound = Rectangle(0, 0, sBound.sz.x/nScale, sBound.sz.y/nScale);

	fp.pFm = GetFileManager();

	{
		SP<InStreamHandler> pFl = fp.ReadFile("config.txt");
		std::istream& ifs = pFl->GetStream();

        if(ifs.fail())
            throw SimpleException("SnakeGameGlobalController", "constructor", "Need config.txt file!\n");

        ifs >> fp;
    }

    
	
    pPr = new MyPreloader(pGr, pSndMng, fp);
    MyPreloader& pr = *pPr;

	//move to main
	//GlobalSoundPlayer::setSoundManager(pSndMng.GetRawPointer());
	//GlobalSoundPlayer::setPreLoader(pPr);

    pr.AddTransparent(Color(0,0,0));
    pr.SetScale(nScale);
    
    pr.LoadTS("pointer.bmp", "pointer", Color(0,0,0), nScale*2);
    pr.LoadTS("boulder.bmp", "boulder");
    pr.LoadTS("wall.bmp", "wall");
    pr.LoadTS("allmouse/mouse_right_0.bmp", "mouse_right_0");
    pr.LoadTS("allmouse/mouse_right_1.bmp", "mouse_right_1");
    pr.LoadTS("allmouse/mouse_left_0.bmp", "mouse_left_0");
    pr.LoadTS("allmouse/mouse_left_1.bmp", "mouse_left_1");
    pr.LoadTS("snake2.bmp", "snake_over");
    pr.LoadTS("logo/anboplu.bmp", "anboplu");

	pr.LoadTS("final/easydead.bmp", "death_1");
    pr.LoadTS("final/mediumdead.bmp", "death_2");
    pr.LoadTS("final/harddead.bmp", "death_3");

	//deathsplosion
	pr.LoadTS("final/explosionB.bmp", "explosion1");
	pr.LoadTS("final/explosionW.bmp", "explosion2");
    
	pr.LoadTS("cut/pushstart1.bmp", "pushstart", Color(0,0,0), nScale*4);
	pr.LoadTS("cut/p1wins1.bmp", "p1wins", Color(0,0,0), nScale*4);
	pr.LoadTS("cut/p2wins1.bmp", "p2wins", Color(0,0,0), nScale*4);
	pr.LoadTS("cut/tiegame1.bmp", "tiegame", Color(0,0,0), nScale*4);

	pr.LoadTS("intro/intro.bmp", "arcade_intro", Color(0,0,0), nScale*4);

	pr.LoadSeqTS("win/win.txt", "win", Color(0,0,0), nScale*2);
    pr.LoadSeqTS("lose/lose.txt", "over", Color(0,0,0), nScale*4);

    pr.LoadSeqTS("snakebody/snakebody.txt", "snake_body");

    pr.LoadSeqTS("logo/gengui.txt", "gengui", Color(0,0,0), nScale*4);
    pr.LoadSeqTS("logo_snake/snakelogo.txt", "snakelogo", Color(0,0,0), nScale*4);
    pr.LoadSeqTS("border/borders.txt", "border", Color(0,0,0), nScale*4);

    pr.LoadSeqTS("dialog/readygo.txt", "readygo", Color(2,2,2), nScale*4);
    pr.LoadSeqTS("dialog/continue.txt", "continue", Color(2,2,2), nScale*4);
    pr.LoadSeqTS("dialog/countdown.txt", "countdown", Color(1,1,1), nScale*4);
    pr.LoadSeqTS("dialog/challenger.txt", "challenger", Color(2,2,2), nScale*4);

	pr.LoadSndSeq("sound/click.txt", "click");

    pr.LoadSnd("sound/die.wav", "die");
	pr.LoadSnd("sound/aSounds/gameover.aif", "gameover");
    //pr.LoadSnd("sound/eat_mouse.wav", "mouse");
    pr.LoadSnd("sound/aSounds/crumble.wav", "crumble");
	pr.LoadSnd("sound/aSounds/explosion.aif", "deathsplosion");

	//pr.LoadSnd("sound/go_invis.wav", "invisible");
    //pr.LoadSnd("sound/go_visible.wav", "visible"); 

	pr.LoadSnd("sound/aSounds/turnInvisible.aif", "invisible");
    pr.LoadSnd("sound/aSounds/leaveInvisible.aif", "visible");
    
	pr.LoadSnd("sound/crossover.wav", "crossover");
    
	//pr.LoadSnd("sound/start_game.wav", "start_game");
	pr.LoadSnd("sound/aSounds/start.wav", "start_game");
    
	//menu sounds
	//pr.LoadSnd("sound/aSounds/menuDown.aif", "menu_down");
	//pr.LoadSnd("sound/aSounds/menuUp.aif", "menu_up");
	pr.LoadSnd("sound/aSounds/menuLeft.aif", "menu_left");
	//pr.LoadSnd("sound/aSounds/start2.aif", "menu_up");

	pr.LoadSnd("sound/aSounds/upchord.wav", "menu_select");
	pr.LoadSnd("sound/aSounds/downchord.wav", "menu_back");

	//pr.LoadSnd("sound/win_level.wav", "win_level");
	pr.LoadSnd("sound/aSounds/eats/C_1_fin.wav", "win_level");

	//Alex's sounds loaded
	//pr.LoadSnd("sound/aSounds/coolNoize.aif", "menu_back");

	pr.LoadSnd("sound/aSounds/wasteland01.wav", "bg_music");
	pr.LoadSnd("sound/aSounds/bg_perc_01.wav", "menu_music");

	//cut scene music
	pr.LoadSnd("sound/aSounds/cutscenes/cs_01.wav", "cutscene_1");
	pr.LoadSnd("sound/aSounds/cutscenes/cs_02.wav", "cutscene_2");
	pr.LoadSnd("sound/aSounds/cutscenes/cs_03.wav", "cutscene_3");
	pr.LoadSnd("sound/aSounds/cutscenes/cs_04.wav", "cutscene_4");

	pr.LoadSnd("sound/aSounds/eats/C_0.wav", "eat_01");
	pr.LoadSnd("sound/aSounds/eats/Db_0.wav", "eat_02");
	pr.LoadSnd("sound/aSounds/eats/D_0.wav", "eat_03");
	pr.LoadSnd("sound/aSounds/eats/Eb_0.wav", "eat_04");
	pr.LoadSnd("sound/aSounds/eats/E_0.wav", "eat_05");
	pr.LoadSnd("sound/aSounds/eats/F_0.wav", "eat_06");
	pr.LoadSnd("sound/aSounds/eats/Gb_0.wav", "eat_07");
	pr.LoadSnd("sound/aSounds/eats/G_0.wav", "eat_08");
	pr.LoadSnd("sound/aSounds/eats/Ab_0.wav", "eat_09");
	pr.LoadSnd("sound/aSounds/eats/A_0.wav", "eat_10");
	pr.LoadSnd("sound/aSounds/eats/Bb_0.wav", "eat_11");
	pr.LoadSnd("sound/aSounds/eats/B_0.wav", "eat_12");
#define N_EAT_SOUNDS 12

	pr.LoadSnd("sound/aSounds/turn01.wav", "turn01");
	pr.LoadSnd("sound/aSounds/turn02.wav", "turn02");
	pr.LoadSnd("sound/aSounds/turn03.wav", "turn03");
	pr.LoadSnd("sound/aSounds/turn04.wav", "turn04");

	SnakeImages snDt;
	
	//load in the snake
	std::vector<std::string> characters;
	characters.push_back("U");
	characters.push_back("R");
	characters.push_back("L");
	characters.push_back("D");
	characters.push_back("N");
	for(unsigned i = 0; i < characters.size(); i++)
	{
		for(unsigned j = 0; j < characters.size(); j++)
		{
			if(characters[i] != characters[j])
			{
				std::string suff = characters[i]+characters[j];
				std::string skeleSuff = characters[i]+characters[j];
				pr.LoadTS("allsnake/"+suff+".bmp","SNAKE"+suff);
				pr.LoadTS("allsnake/"+suff+".bmp","RED_SNAKE"+suff);
				pr.LoadTS("skeleSnake/"+suff+".bmp","SKELESNAKE"+suff);

				snDt.stImg.insert("SNAKE"+suff);

				pGr->GetImage(pr["RED_SNAKE"+suff])->ChangeColor(Color(0, 255, 0), Color(255, 0, 0));
			}
		}
	}

	plr.pSnd = pSndMng;
	
	plr.vThemes.resize(2);
	plr.vThemes[BG_BACKGROUND] = pr.GetSnd("bg_music");
	plr.vThemes[BG_MENU] = pr.GetSnd("menu_music");
	       

    MyCamera cc    (pGr, Scale(float(nScale), rBound));
    MyCamera cc_big(pGr, Scale(float(nScale), rBound));
    cc_big.Zoom(2);

    
    pFont = new FontWriter(fp, "snakefont/snakefont2.txt", pGr, nScale);
    pFontBig = new FontWriter(fp, "snakefont/snakefont2.txt", pGr, nScale*2);

    //Size szTotal = Size(30, 20);
    Size szElement = Size(20, 20);

	//SP<Event> UpSound;
	//SP<Event> DownSound;
	//SP<Event> LeftSound;
	//SP<Event> RightSound;
	//SP<Event> InSound;
	//SP<Event> OutSound;

	MenuSounds msnd(  new MakeSoundEvent(pSndMng, pr.GetSnd("menu_left"))
					, new MakeSoundEvent(pSndMng, pr.GetSnd("menu_left"))
					, new MakeSoundEvent(pSndMng, pr.GetSnd("menu_left"))
					, new MakeSoundEvent(pSndMng, pr.GetSnd("menu_left"))
					, new MakeSoundEvent(pSndMng, pr.GetSnd("menu_select"))
					, new MakeSoundEvent(pSndMng, pr.GetSnd("menu_back")));
    
    LevelDefinition lvcr;
	lvcr.nLength = 20;
	lvcr.nDisplayLength = 10;
	lvcr.nDir = 2;
	lvcr.pInPos = Point(10, 21);
	lvcr.nSpeed = 4;
	lvcr.bInv = false;
	lvcr.mtxObjects = Matrix<int>(Size(30, 30));
	
	LevelInfo linf;
	linf.nScore = 0;
	linf.nLives = 1;
	linf.lvCurr = lvcr;


	SP<ActualGame> pBackgroundGame = new ActualGame(cc, szElement, Background(sBound), pr, pFont, pFontBig, linf, 0, &pCurrControl, pHigh, LevelProgressUpdater(0, lp), pSel.GetRawPointer(), pSndMng, InfoPack(0, gd, snDt, true), true);

	int nS = 4;
	int nL = 18;
	int nH = 14;
	int nT = 1;
	
	for(int i = 0; i < nL*2 + nH*2; ++i)
	{
		if(i == nS)
		{
			AddCycle(0, 3, pBackgroundGame->vKeys);
			pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
		}
		else if(i == nS + nT)
		{
			pBackgroundGame->vKeys.push_back(GuiKeyType(' '));
		}
		else if(i == nS + nH)
		{
			AddCycle(3, 3, pBackgroundGame->vKeys);
			pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
		}
		else if(i == nS + nH + nT)
		{
			pBackgroundGame->vKeys.push_back(GuiKeyType(' '));
		}
		else if(i == nS + nH + nL)
		{
			AddCycle(2, 3, pBackgroundGame->vKeys);
			pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
		}
		else if(i == nS + nH + nL + nT)
		{
			pBackgroundGame->vKeys.push_back(GuiKeyType(' '));
		}
		else if(i == nS + nH + nL + nH)
		{
			AddCycle(1, 3, pBackgroundGame->vKeys);
			pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
		}
		else if(i == nS + nH + nL + nH + nT)
		{
			pBackgroundGame->vKeys.push_back(GuiKeyType(' '));
		}
		else
			pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
		pBackgroundGame->vKeys.push_back(GuiKeyType('a'));
	};
	
	SparseVector(pBackgroundGame->vKeys);
	//SparseVector(pBackgroundGame->vKeys);

	pMenu = new MenuController(cc_big, pFontBig, pr["pointer"], Background(sBound), pExitProgram, false//gd == GD_MOBILE ? true : false
		, msnd, pBackgroundGame);

	pMenu->hd = HintsDisplay(gd);
	pMenu->bHints = true;
	pMenu->pHntFnt = pFont;

    pHigh = new HighScoreController(pFontBig, cc_big, NewSwitchEvent(pCurrControl, pMenu), 0, Background(sBound), fp);
	pHigh->msnd = msnd;

    pCntr = new ControlsController(pFontBig, NewSwitchEvent(pCurrControl, pMenu), Background(sBound), cc_big);
	pCntr->msnd = msnd;

    pDiffMenu =
        new MenuController(cc_big, pFontBig, pr["pointer"], Background(sBound), NewSwitchEvent(pCurrControl, pMenu)
		, false //gd == GD_MOBILE ? true : false
		, msnd  //Menu move sound
		);
    
	pArcadeIntro = new EventAnimationController(cc, pSndMng, sBound, Color(0,0,0),
        pExitProgram, true);
    
	pArcadeIntro->NewAnimation(new StaticImage(Center(rBound), pr["arcade_intro"], true));

	pArcadeIntro->nTheme = BG_MENU;

	SP<Event> pBackToMenuSwitch = new SequenceOfEvents(
		new HintChangeEvent(pMenu->hd),
		NewSwitchEvent(pCurrControl, pMenu));

	if(gd == GD_ARCADE)
		pBackToMenuSwitch = NewSwitchEvent(pCurrControl, pArcadeIntro);


	LevelInfo2 lvlMult;
	lvlMult.Read(cc, fp, "_mult");
	lvlMult.LoadLevel(1);

    pTwoPlayerGameEvent = new NewGameEvent2(&pCurrControl, pBackToMenuSwitch,
		cc, szElement, Background(sBound), pr, pFont, pFontBig, lvlMult, pHigh, LevelProgressUpdater(0, lp), pSel.GetRawPointer(), pSndMng, snDt, InfoPack2(gd));

	SP<NewGameEvent2> pGmMult = new NewGameEvent2(&pCurrControl, pBackToMenuSwitch,
		cc, szElement, Background(sBound), pr, pFont, pFontBig, lvlMult, pHigh, LevelProgressUpdater(0, lp), pSel.GetRawPointer(), pSndMng, snDt, InfoPack2(gd));

	InfoPack ipck(&pTwoPlayerGameEvent, gd, snDt, true);

	SP<NewGameEvent> pGmEv = new NewGameEvent(&pCurrControl, pBackToMenuSwitch,
        cc, szElement, Background(sBound), pr, pFont, pFontBig, LevelInfo(), pHigh, LevelProgressUpdater(-1, lp), 0, pSndMng, ipck);

    pSel = new LevelSelectController(
        NewSwitchEvent(pCurrControl, pMenu), pGmEv, pFontBig, Background(sBound), fp, lp, cc_big, msnd, false /*gd == GD_MOBILE ? true : false*/, gd);
    pGmEv->pSel = pSel.GetRawPointer();

    SP<NewGameEvent> pGmEvE = new NewGameEvent(&pCurrControl, pBackToMenuSwitch,
        cc, szElement, Background(sBound), pr, pFont, pFontBig, pSel->lvlE, pHigh, LevelProgressUpdater(0, lp), pSel.GetRawPointer(), pSndMng, ipck);
    SP<NewGameEvent> pGmEvM = new NewGameEvent(&pCurrControl, pBackToMenuSwitch,
        cc, szElement, Background(sBound), pr, pFont, pFontBig, pSel->lvlM, pHigh, LevelProgressUpdater(1, lp), pSel.GetRawPointer(), pSndMng, ipck);
    SP<NewGameEvent> pGmEvH = new NewGameEvent(&pCurrControl, pBackToMenuSwitch,
        cc, szElement, Background(sBound), pr, pFont, pFontBig, pSel->lvlH, pHigh, LevelProgressUpdater(2, lp), pSel.GetRawPointer(), pSndMng, ipck);

    pCred = new CreditsController(pFontBig, pBackToMenuSwitch, Background(sBound), cc_big);
	pCred->msnd = msnd;
	std::cout << "1" << std::endl;
    pMenu->AddEntry("Start", NewSwitchEvent(pCurrControl, pDiffMenu));
#ifdef ENABLE_MULTIPLAYER
	pMenu->AddEntry("Multiplayer", pGmMult);
#endif
    pMenu->AddEntry("Level Select", NewSwitchEvent(pCurrControl, pSel));
    std::cout << "2" << std::endl;
	if(gd == GD_PC)
		pMenu->AddEntry("Controls", new ControlsSwitchEvent(pCurrControl, pCntr));
    std::cout << "3" << std::endl;
	pMenu->AddEntry("High Scores", NewSwitchEvent(pCurrControl, pHigh));
    pMenu->AddEntry("Credits", NewSwitchEvent(pCurrControl, pCred));
    //pMenu->AddEntry("Exit", pExitProgram);
	std::cout << "4" << std::endl;
    pCutE = new CutSceneController(pSndMng, pFontBig, cc, pGmEvE, Background(sBound), 0, pr, true);
    pCutM = new CutSceneController(pSndMng, pFontBig, cc, pGmEvM, Background(sBound), 5, pr, true);
    pCutH = new CutSceneController(pSndMng, pFontBig, cc, pGmEvH, Background(sBound), 10, pr, true);
	std::cout << "5" << std::endl;
    pDiffMenu->AddEntry("Easy",   new SequenceOfEvents(NewSwitchEvent(pCurrControl, pCutE),
        new MakeSoundEvent(pSndMng, pr.GetSnd("start_game")) ));
    pDiffMenu->AddEntry("Medium", new SequenceOfEvents(NewSwitchEvent(pCurrControl, pCutM),
        new MakeSoundEvent(pSndMng, pr.GetSnd("start_game")) ));
    pDiffMenu->AddEntry("Hard",   new SequenceOfEvents(NewSwitchEvent(pCurrControl, pCutH),
        new MakeSoundEvent(pSndMng, pr.GetSnd("start_game")) ));
	std::cout << "6" << std::endl;
	pCutE->SetNewGameEvent(pGmEvE);
	std::cout << "7" << std::endl;
	pArcadeIntro->mpKeyEvents[GuiKeyType('1')] = new SequenceOfEvents(NewSwitchEvent(pCurrControl, pCutE),
        new MakeSoundEvent(pSndMng, pr.GetSnd("start_game")), NewCpSwitchEvent(pCutE->pNewGameAux->ipck.bPlayer1, true) );
	
	pArcadeIntro->mpKeyEvents[GuiKeyType('2')] = new SequenceOfEvents(NewSwitchEvent(pCurrControl, pCutE),
        new MakeSoundEvent(pSndMng, pr.GetSnd("start_game")), NewCpSwitchEvent(pCutE->pNewGameAux->ipck.bPlayer1, false) );

	std::cout << "8" << std::endl;
    pInit3 = new EventAnimationController(cc, pSndMng, sBound, Color(0,0,0),
		pBackToMenuSwitch, false /*gd == GD_MOBILE ? true : false*/);
    
    pInit2 = new EventAnimationController(cc, pSndMng, sBound, Color(0,0,0),
        NewSwitchEvent(pCurrControl, pInit3), false /*gd == GD_MOBILE ? true : false*/);

	pInit0 = new AnbopluLogoController(cc,pSndMng,NewSwitchEvent(pCurrControl, pInit2), pr["anboplu"], pr,
		false /*gd == GD_MOBILE ? true : false*/);
	std::cout << "9" << std::endl;
    pCurrControl = pInit0;

    SP<AnimationOnce> pAn2 = new AnimationOnce(pr("gengui"), Center(rBound), (100/nFrameRate)*1, true,
        NewTerminatorEvent(pInit2.GetRawPointer()));

    pInit2->NewAnimation(pAn2);
	std::cout << "10" << std::endl;
    SP<SoundOnce> pSnd2 = new SoundOnce(pr.GetSndSeq("click"), (100/nFrameRate)*1, new EmptyEvent());
	std::cout << "10.5" << std::endl;
    pInit2->NewAnimation(pSnd2);
	std::cout << "10.52" << std::endl;
    SP<AnimationOnce> pAn3 = new AnimationOnce(pr("snakelogo"), Center(rBound), (100/nFrameRate)*1, true,
        NewTerminatorEvent(pInit3.GetRawPointer()));
	std::cout << "10.53" << std::endl;
    pInit3->NewAnimation(pAn3);
	std::cout << "11" << std::endl;
}

SnakeGameGlobalController::~SnakeGameGlobalController()
{
	plr.vThemes.clear();
	plr.pSnd = 0;
}


void SnakeGameGlobalController::Update()
{
    pCurrControl->Update();
}

void SnakeGameGlobalController::KeyDown(GuiKeyType nCode)
{
    if(gd == GD_MOBILE)
		if(bIgnoreSpaceReturn)
			if(nCode == GuiKeyType(' ') || nCode == GUI_RETURN)
				return;
	
	pCurrControl->OnKey(nCode, false);
}

void SnakeGameGlobalController::KeyUp(GuiKeyType nCode)
{
    pCurrControl->OnKey(nCode, true);
}

void SnakeGameGlobalController::MouseClick(Point p)
{
	if(gd == GD_MOBILE)
		pCurrControl->OnClick(p);
}



///////////////////////////////////////////////////////////////////
///////// <START: Alex's sound code> //////////////////////////////
///////////////////////////////////////////////////////////////////
void ActualGame::playEatSound()
{
	//int totalMice = lvl.lvCurr.nMice;
	int soundIndex = 12 - gd.nMiceRemaining() - 1;
	playEatSound(soundIndex);
}

void ActualGame::playEatSound(int index)
{
	int adjustedIndex = std::max(0, std::min(index, N_EAT_SOUNDS));

	std::string eatFileStr;
	if (adjustedIndex < 10)
		eatFileStr = "eat_0" + S(adjustedIndex);
	else
		eatFileStr = "eat_" + S(adjustedIndex);

	pSndMng->PlaySound(pr.GetSnd(eatFileStr));
}
///////////////////////////////////////////////////////////////////
///////// <END: Alex's sound code> ////////////////////////////////
///////////////////////////////////////////////////////////////////
//class GlobalSoundPlayer;
void AnbopluLogoController::Update()
	{
		drawLogo(counter);
		counter++;
		//GlobalSoundPlayer::stopSound(BG_MUSIC_CHANNEL);
		//GlobalSoundPlayer::playSound("menu_music", BG_MUSIC_CHANNEL, true);
		//pSnd->PlaySound(pr.GetSnd("menu_music"), BG_MUSIC_CHANNEL, true);
		plr.SwitchTheme(BG_MENU);
	}

void BackgroundMusicPlayer::SwitchTheme(int nTheme)
{
	if(nCurrTheme == nTheme)
		return;

	if(nTheme < 0 || nTheme >= int(vThemes.size()))
		throw SimpleException("BackgroundMusicPlayer", "SwitchTheme", "Bad theme index " + S(nTheme));

	pSnd->PlaySound(vThemes[nTheme], BG_MUSIC_CHANNEL, true);

	nCurrTheme = nTheme;
}

void BackgroundMusicPlayer::StopMusic()
{
	if(nCurrTheme != -1)
		pSnd->StopSound(BG_MUSIC_CHANNEL);
	nCurrTheme = -1;
}

BackgroundMusicPlayer plr;


// Two-player stuff

Snake2::Snake2(ActualGame2* pGm_, int nNum, SnakeImages snDt, Color c)
:bDead(false), pDir(GetDir(pGm_->lvl.lvCurr.nDir)), tMove(pGm_->lvl.lvCurr.nSpeed), bInvisible(false), nGrow(0), /*bBoomLock(false),*/ pGm(pGm_)// tAlternate(1), bRed(false), bDamageFudgeCnt(false)
, nBoomCounter(-1)//, bKeyLock(false)
{
	if(nNum == 1)
	{
		pDir = GetDir(pGm_->lvl.lvCurr.nDir2);
		//tMove = Timer(15);
	}
	
	m_deathAnimCounter = 0;
	m_deathAnimState = STATE_PRE_DEATH_PAUSE;
	//m_invisibleFlickerTime = FLICKER_DURATION;
	//m_delayToDamage = DELAY_TO_DAMAGE;
    
	unsigned nLength = pGm->lvl.lvCurr.nLength;
    
	if (nLength > pGm->lvl.lvCurr.nDisplayLength)
	{
		nGrow = nLength - pGm->lvl.lvCurr.nDisplayLength;
		nLength = pGm->lvl.lvCurr.nDisplayLength;
	}
    
	Point p1 = pGm->lvl.lvCurr.pInPos;
	Point p2 = -GetDir(pGm->lvl.lvCurr.nDir);

	if(nNum == 1)
	{
		p1 = pGm->lvl.lvCurr.pInPos2;
		p2 = -GetDir(pGm->lvl.lvCurr.nDir2);
	}

    for(unsigned i = 0; i < nLength; ++i)
    {
        ++pGm->gd.mtx.at(p1).nSnakeLayers;
        lsPos.push_back(p1);
		p1 += p2;
    }

	for(std::set<std::string>::iterator itr = snDt.stImg.begin(), etr = snDt.stImg.end(); itr != etr; ++itr)
	{
		std::string sName = *itr;
		Index img = pGm->pr[sName];
		Index imgCp = pGm->cc.pGr->CopyImage(img);
		pGm->cc.pGr->GetImage(imgCp)->ChangeColor(Color(0, 255, 0), c);

		mpSnakeImages[sName] = imgCp;
	}
}

int Snake2::getDelayToDamage()
{
	//easy slow nSpeed ~= 5
	//medium fast nSpeed ~= 2
	return 7;//ceil(10.0 / pGm->lvl.lvCurr.nSpeed) + 1; //3 seconds, just to test accuracy
}

void Snake2::stepDeathAnimation()
{
	m_deathAnimCounter++;
	if (
		m_deathAnimState == STATE_PRE_DEATH_PAUSE && m_deathAnimCounter == PRE_DEATH_ANIM_PAUSE
		||
		m_deathAnimState == STATE_DISENTGRATE_BODY && m_deathAnimCounter >= (lsPos.size() - 1)*DEATH_STEP_PAUSE
		||
		m_deathAnimState == STATE_PRE_DISINTEGRATE_HEAD && m_deathAnimCounter == PRE_SKULL_PAUSE
		||
		m_deathAnimState == STATE_POST_DEATH_PAUSE && m_deathAnimCounter == POST_DEATH_PAUSE_LENGTH
		)
	{
		m_deathAnimCounter = 0;
		m_deathAnimState++;
	}

	if (m_deathAnimState == STATE_DISENTGRATE_BODY &&  m_deathAnimCounter%DEATH_STEP_PAUSE == DEATH_STEP_PAUSE - 1)
	{
		pGm->pSndMng->StopSound(5);
		pGm->pSndMng->PlaySound(pGm->pr.GetSnd("crumble"),5);
	} 
	else if (m_deathAnimState == STATE_POST_DEATH_PAUSE && m_deathAnimCounter == 0)
	{
		pGm->pSndMng->PlaySound(pGm->pr.GetSnd("deathsplosion"));
	}
	
}

Point Snake2::GetMove()
{
	Point pNextDir;
	if(!lsDirBuff.empty())
	{
		pNextDir = lsDirBuff.front();
		lsDirBuff.pop_front();
	}
	else
		pNextDir = pDir;
	
	if(pDir != -pNextDir)
		pDir = pNextDir;

	if(pNextDir.x != pDir.x && pNextDir.y != pDir.y)// && !bBoomLock && m_damageFudge == getDelayToDamage())
	{
		if (pNextDir.x == -1)
		{
			pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn01"));
		} 
		else if (pNextDir.y == 1)
		{
			pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn02"));
		} 
		else if (pNextDir.x == 1)
		{
			pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn03"));
		} 
		else if (pNextDir.y == -1)
		{
			pGm->pSndMng->PlaySound(pGm->pr.GetSnd("turn04"));
		} 
	}
	
    
	Point p = lsPos.front() + pDir;
    


	pGm->gd.Teleport(p);

	return p;
}

void Snake2::DoMove(Point p, bool bIgnoreTailErase)
{
	if(pGm->gd.mtx.at(p).nSnakeLayers != 0 && !bIgnoreTailErase)
	{
		pGm->pSndMng->PlaySound(pGm->pr.GetSnd("crossover"));
	}

	if(pGm->gd.mtx.at(p).pMouse)
	{
		pGm->playEatSound(pGm->nCombo + 1);
		
		pGm->m_comboClock += pGm->getComboTimeBonus();


		pGm->gd.mtx.at(p).pMouse->bDead = true;
		pGm->gd.mtx.at(p).pMouse = 0;
		pGm->gd.UnSet(p);
		++nGrow;
		pGm->lvl.nScore += 100 * (pGm->nCombo + 1);

		if (pGm->m_comboClock > 0)
		{
			pGm->nCombo++;
		}
	}

	lsPos.push_front(p);
	++pGm->gd.mtx.at(p).nSnakeLayers;
    
	if(nGrow == 0)
	{
		--pGm->gd.mtx.at(lsPos.back()).nSnakeLayers;

		if(!bIgnoreTailErase)
			pGm->gd.UnSet(lsPos.back());

		lsPos.pop_back();
		
	}
	else
		--nGrow;

	Draw();
}

void Snake2::NewDir(Point pNewDir)
{
	lsDirBuff.push_back(pNewDir);
	while(lsDirBuff.size() > 2)
		lsDirBuff.pop_front();
}

std::string Snake2::getSnakeChar(Point b, Point f)
{
	switch(f.x-b.x)
	{
	case 0:
		switch(f.y-b.y)
		{
		case 1:
			return "D";
			break;
		case -1:
			return "U";
			break;
		default:
			return f.y-b.y<-1?"D":"U";
			break;
		}
		break;
	case 1:
		return "R";
		break;
	case -1:
		return "L";
		break;
	default:
		return f.x-b.x<-1?"R":"L";
		break;
	}
}
	
void Snake2::Draw()
{
	int segIdx = lsPos.size();

	for(std::list<Point>::iterator itr = lsPos.begin(), etr = lsPos.end(); itr != etr; ++itr)
	{
		bool deadHere = false;    
		if(pGm->gd.mtx.at(*itr).nSnakeLayers == 1)
		{
			//if(!bInvisible || bRed || pGm->lvl.nLives == 0)
			if(!bInvisible || pGm->lvl.nLives == 0)
			{

				std::string word("");
				//--means to the head, ++ means to tail

				
				if(lsPos.size() > 1)
				{
					//go towards tail
					std::list<Point>::iterator b = ++itr;
					itr--;
					if(b == etr)
						word+="N";
					else
						word+=getSnakeChar(*itr,*b);


					//check if head
					if(itr == lsPos.begin())
						word+="N";
					else
					{
						std::list<Point>::iterator f = --itr;
						itr++;
						word+=getSnakeChar(*itr,*f);
					}
				}
				else
				{
					word += getSnakeChar(pDir, Point(0,0));
					word += "N";
				}



				if (bDead)
				{
					if (segIdx > nSkipSegments())
					{
						pGm->gd.mtx.at(*itr).img = pGm->pr["SKELESNAKE"+word];
						pGm->gd.mtx.at(*itr).bDraw = true;
					} 
					else
					{
						deadHere = true;
						pGm->gd.mtx.at(*itr).nSnakeLayers = 1;
					}
				} 
				else 
				{
					if(mpSnakeImages.find("SNAKE"+word) == mpSnakeImages.end())
						throw SimpleException("Snake2", "Draw", "Cannot load predrawn image " + std::string("SNAKE" + word));
					
					pGm->gd.mtx.at(*itr).img = mpSnakeImages["SNAKE"+word];
					pGm->gd.mtx.at(*itr).bDraw = true;
				}

			}
			else
			{
				pGm->gd.UnSet(*itr);
			}
		}

		if(pGm->gd.mtx.at(*itr).nSnakeLayers > 1)
		{
			//pGm->gd.mtx.at(*itr).img = pGm->pr["snake_over"];
			//pGm->gd.mtx.at(*itr).bDraw = true;
			if (segIdx > nSkipSegments())
			{
				pGm->gd.Set(*itr, pGm->pr["snake_over"]);
			} 
			else 
			{
				//figure out if this crossover is still being drawn further up the chain. otherwise erase it.
				int segIdx2 = segIdx;
				bool drawAnyway = false;
				for (std::list<Point>::iterator checkAheadItr(itr); checkAheadItr != lsPos.end(); checkAheadItr++)
				{
					if (*checkAheadItr == *itr && segIdx2 != segIdx && segIdx2 <= nSkipSegments())
					{
						drawAnyway = true;
						break;
					}
					segIdx2 --;
				}

				if (drawAnyway)
				{
					pGm->gd.mtx.at(*itr).UnSetImage();
					pGm->gd.mtx.at(*itr).bDraw = false;
				}

			}
		}


		if (deadHere)
		{
			pGm->gd.mtx.at(*itr).UnSetImage();
			pGm->gd.mtx.at(*itr).bDraw = false;
		}

		segIdx--;
	}

	if (m_deathAnimState == STATE_POST_DEATH_PAUSE)
	{
		Gui::Point headPt = *(lsPos.begin());
		if (m_deathAnimCounter == 0)
		{
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					Gui::Point areaPt(headPt.x + i, headPt.y + j);
					Rectangle r(pGm->gd.szTotal);
					if(InsideRectangle(r, areaPt))
						pGm->gd.mtx.at(areaPt).UnSetImage();
					//pGm->gd.mtx.at(areaPt).bDraw = false;
				}
			}
		}
		//headPt.x -=1;
		//headPt.y -=1;
		if (m_deathAnimCounter <= 6)
		{
			if (m_deathAnimCounter <= 1)
			{
				pGm->gd.mtx.at(headPt).img = pGm->pr["explosion1"];
				pGm->gd.mtx.at(headPt).bCenter = true;
			}
			else
			{
				pGm->gd.mtx.at(headPt).img = pGm->pr["explosion2"];
				pGm->gd.mtx.at(headPt).bCenter = true;
			}
			pGm->gd.mtx.at(headPt).bDraw = true;
		}
		else
		{
			pGm->gd.mtx.at(headPt).UnSetImage();
			pGm->gd.mtx.at(headPt).bDraw = false;
		}
	}
}

void SnakeMoveDescriptor::ProcessPoint(std::vector<SP<Snake2> >& vSnakes, std::vector<SnakeMoveDescriptor>& vSnakeDescr)
{
	for(size_t i = 0, sz = vSnakes.size(); i < sz; ++i)
	{
		std::list<Point>::iterator itr_ls = vSnakes[i]->lsPos.end();
		--itr_ls;

		for(std::list<Point>::iterator itr = vSnakes[i]->lsPos.begin(), etr = vSnakes[i]->lsPos.end(); itr != etr; ++itr)
			if(*itr == p)
			{
				vMoves.push_back(PointToMove(!vSnakes[i]->bInvisible, itr == itr_ls, i));

				if(itr == itr_ls)
					vSnakeDescr[i].bTailMovement = true;
			}
	}

	if(vMoves.empty())
	{
		bUnclear = false;
		bLose = false;
	}
	else
	{
		bUnclear = false;
		bLose = false;

		for(size_t i = 0; i < vMoves.size(); ++i)
		{
			if(!vMoves[i].bVisible)
				continue;

			if(vMoves[i].bVisible && (!vMoves[i].bTail || vSnakes[vMoves[i].nTargetSnake]->nGrow))
			{
				bUnclear = false;
				bLose = true;
				break;
			}
			
			if(vMoves[i].bVisible && vMoves[i].bTail)
			{
				bUnclear = true;
				continue;
			}
		}
	}
}

void ProcessHeadCollisions(std::vector<SnakeMoveDescriptor>& vSnakeDescr)
{
	size_t sz = vSnakeDescr.size();
	for(size_t i = 0; i < sz; ++i)
	{
		if(!vSnakeDescr[i].bMoving)
			continue;
		
		for(size_t j = 0; j < sz; ++j)
		{
			if(i == j)
				continue;

			if(!vSnakeDescr[j].bMoving)
				continue;

			if(vSnakeDescr[j].pSn->bInvisible)
				continue;

			if(vSnakeDescr[i].p == vSnakeDescr[j].p)
			{
				vSnakeDescr[i].bUnclear = false;
				vSnakeDescr[i].bLose = true;
				break;
			}
		}
	}
}

void SnakeMoveDescriptor::ProcessTails(std::vector<SnakeMoveDescriptor>& vSnakeDescr)
{
	for(unsigned i = 0; i < vSnakeDescr.size(); ++i)
		vSnakeDescr[i].bInternalProcessed = false;

	std::list<SnakeMoveDescriptor*> lsStack;

	bInternalProcessed = true;
	lsStack.push_back(this);

	for(std::list<SnakeMoveDescriptor*>::iterator itr = lsStack.begin(), etr = lsStack.end(); itr != etr; ++itr)
	{
		SnakeMoveDescriptor* pSmd = *itr;
		pSmd->bInternalProcessed = true;

		for(unsigned i = 0; i < pSmd->vMoves.size(); ++i)
		{
			SnakeMoveDescriptor* pSmdMv = &vSnakeDescr[pSmd->vMoves[i].nTargetSnake];

			if(pSmdMv->bInternalProcessed)
				continue;
			
			if(!pSmd->vMoves[i].bVisible || !pSmd->vMoves[i].bTail)
				continue;

			if(!pSmdMv->bMoving)
			{
				bUnclear = false;
				bLose = true;
				
				return;
			}

			if(!pSmdMv->bUnclear)
			{
				if(pSmdMv->bLose)
				{
					bUnclear = false;
					bLose = true;
					
					return;
				}
			}

			pSmdMv->bInternalProcessed = true;
			lsStack.push_back(pSmdMv);
		}
	}

	bUnclear = false;
	bLose = false;
}

ActualGame2::ActualGame2(MyCamera cc_, Size szElement_, Background bkgr_, MyPreloader& pr_, SP<FontWriter> pFont_, SP<FontWriter> pFontCut_, LevelInfo2 lvl_, SP<Event> pLoseEvent_, SP<GameController>* ppCurrControl_, SP<HighScoreController> pHigh_, LevelProgressUpdater lpu_, LevelSelectController* pSel_, SP< SoundManager > pSndMng_, SnakeImages snDt_, InfoPack2 ipck_, bool bPreProgrammed_)
:gd(cc, Size(), szElement_), bkgr(bkgr_), pr(pr_), cc(cc_), pFont(pFont_), pFontCut(pFontCut_), lvl(lvl_), pLoseEvent(pLoseEvent_), ppCurrControl(ppCurrControl_), pDialogBox(0), ipck(ipck_),
szElement(szElement_), pHigh(pHigh_), lpu(lpu_), pSel(pSel_), pSndMng(pSndMng_), ts(TS_PLAYING), nCombo(0), m_comboClock(0), bPreProgrammed(bPreProgrammed_), nKeyPos(0), snDt(snDt_)
{
	InitializeFromLevel();

	lvl.nLives = 1;


	vSnakes.push_back(new Snake2(this, 0, snDt, Color(0, 255, 0)));
	if(!bPreProgrammed)
		vSnakes.push_back(new Snake2(this, 1, snDt, Color(0, 0, 255)));

	//if(lvl.lvCurr.bInv)
    //    pSn->bInvisible = true;

    for(unsigned i = 0; i < vSnakes.size(); ++i)
		vSnakes[i]->Draw();

	if(!bPreProgrammed)
	{
		SP<EventAnimationController> pInit3 = new EventAnimationController(cc, pSndMng, cc.GetBox(), Color(0,0,0),
			NewCpSwitchEvent< SP<GameController>, SP<GameController> >(pDialogBox, 0), false, true);

		SP<AnimationOnce> pAn3 = new AnimationOnce(pr("readygo"), cc.fromF(fPoint(.5,.5)), (100/nFrameRate)*1, true,
			NewTerminatorEvent(pInit3.GetRawPointer()));

		pInit3->NewAnimation(pAn3);

		pDialogBox = pInit3;
	}
}

void ActualGame2::InitializeFromLevel()
{
	Size sz = lvl.lvCurr.mtxObjects.GetSize();
	Point p;

	szTotal = sz;

	gd = Grid(cc, szTotal, szElement);
	
	Size szDr = Size(szTotal.x * szElement.x, szTotal.y * szElement.y);

	gd.cc.BX_Translate(Point((gd.cc.GetBox().x - szDr.x)/2, (gd.cc.GetBox().y - szDr.y)/2));

	for(p.x = 0; p.x < sz.x; ++p.x)
	for(p.y = 0; p.y < sz.y; ++p.y)
	{
		int nCd = lvl.lvCurr.mtxObjects.at(p);
		if(nCd == 0)
			continue;
		
		if(nCd == -1)
			gd.Set(p, pr["wall"], true);
		else if(nCd == -2)
		{
			if(!lvl.lvCurr.bRndBoulders)
				gd.Set(p, pr["boulder"], true);
		}
		else if(!lvl.lvCurr.bRndMice)
		{
			MiceDescr md = lvl.lvCurr.vMice.at(nCd - 1);
			vMice.push_back(new Mouse(p, md.dRate, gd, pr, md.nRange, md.nWallCode, md.fMaxSpeed, md.fRandomWalk));
		}
	}

	if(lvl.lvCurr.bRndBoulders)
	{
		for(int i = 0; i < lvl.lvCurr.nRndWall; ++i)
		{
			Point p;
			p.x = rand()%szTotal.x;
			p.y = rand()%szTotal.y;

			if(!gd.mtx[p].bDraw)
				gd.Set(p, pr["boulder"], true);
		}
	}

	if(lvl.lvCurr.bRndMice)
	{
		for(size_t j = 0; j < lvl.lvCurr.vMice.size(); ++j)
		{
			MiceDescr md = lvl.lvCurr.vMice[j];
			for(int i = 0; i < lvl.lvCurr.vMice[j].nRndNum; ++i)
			{
				Point p;
				p.x = rand()%szTotal.x;
				p.y = rand()%szTotal.y;

				if(!gd.mtx[p].bDraw)
					vMice.push_back(new Mouse(p, md.dRate, gd, pr, md.nRange, md.nWallCode, md.fMaxSpeed, md.fRandomWalk));
			}
		}
	}
}

void ActualGame2::OnKeySnake(GuiKeyType nCode, SP<Snake2> pCurrSn, bool bUp)
{
	bool bDir = (nCode == GUI_UP || nCode == GUI_DOWN || nCode == GUI_LEFT || nCode == GUI_RIGHT);

	if(bUp)
		return;
	
	/*
	if(bUp)
	{
		if(bDir)
			pCurrSn->bKeyLock = false;
		
		return;
	}
	*/
	
	/*
	if(bDir)
	{
		if(pCurrSn->bKeyLock)
			return;

		pCurrSn->bKeyLock = true;
	}
	*/
	

    if(nCode == GUI_ESCAPE)
    {
        Lose(false);
        return;
    }

	if(pDialogBox != 0)
	{
		return;
	}

	Point pNewDir = pCurrSn->pDir;
	Point prevDir = pCurrSn->pDir;

	if(nCode == GUI_UP)
	{
        pNewDir = Point(0,-1);
	}
    else if(nCode == GUI_DOWN)
	{
        pNewDir = Point(0,1);
	}
    else if(nCode == GUI_LEFT)
	{
        pNewDir = Point(-1,0);
	}
    else if(nCode == GUI_RIGHT)
	{
        pNewDir = Point(1,0);
	}
    else if(char(nCode) == ' ')
    {
        pCurrSn->bInvisible = !pCurrSn->bInvisible;

		if (ts == TS_PLAYING) //don't play invisibility sounds on win!
		{
        if(pCurrSn->bInvisible)
            pSndMng->PlaySound(pr.GetSnd("invisible"), 3);
        else
            pSndMng->PlaySound(pr.GetSnd("visible"), 3);
		}
    }
    else if(char(nCode) == '\\')
    {
#ifdef ENABLE_CHEATS
		plr.StopMusic();
		pSndMng->PlaySound(pr.GetSnd("win_level"));
		ts = TS_WIN;
		tTerminal = Timer(WIN_DELAY);
#endif
        //Win(); //TODO: currently win waits another tick so the last mouse actually gets eaten.
		//If there are lots of mice left, then it just doesn't work.
        return;
    }
    else if(char(nCode) == ']')
    {
		++pCurrSn->nGrow;
	}

    if(bDir)
    {
        pCurrSn->NewDir(pNewDir);

		//Play appropriate turn sound.
		/*
		if (pNewDir != pCurrSn->pDir)
		{
		if (pCurrSn->pNextDir.x == -1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn01"));
		} 
		else if (pCurrSn->pNextDir.y == 1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn02"));
		} 
		else if (pCurrSn->pNextDir.x == 1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn03"));
		} 
		else if (pCurrSn->pNextDir.y == -1)
		{
			pSndMng->PlaySound(pr.GetSnd("turn04"));
		} 
		}*/

        //if(bDir)
		//{
        //    pCurrSn->bBoomLock = false;
		//}
    }
}



/*virtual*/ void ActualGame2::OnKey(GuiKeyType nCode, bool bUp)
{
	if(nCode == 'i' || nCode == 'w')
		OnKeySnake(GUI_UP, vSnakes[1], bUp);
	else if(nCode == 'j' || nCode == 'a')
		OnKeySnake(GUI_LEFT, vSnakes[1], bUp);
	else if(nCode == 'k' || nCode == 's')
		OnKeySnake(GUI_DOWN, vSnakes[1], bUp);
	else if(nCode == 'l' || nCode == 'd')
		OnKeySnake(GUI_RIGHT, vSnakes[1], bUp);
	else if(nCode == GUI_RETURN || nCode == 'v')
		OnKeySnake(GuiKeyType(' '), vSnakes[1], bUp);
	else if(nCode == '/')
		OnKeySnake(GuiKeyType(' '), vSnakes[0], bUp);
	else
		OnKeySnake(nCode, vSnakes[0], bUp);
}

/*virtual*/ void ActualGame2::OnClick(Point p)
{
	//OnKey(GuiKeyType(' '), false);
}

/*virtual*/ void ActualGame2::Update()
{
	if(!bPreProgrammed)
	{
		bkgr.Draw(gd.cc);

		bIgnoreSpaceReturn = false;

		gd.cc.Push();

		gd.cc.BX_Translate(Point(-1,-1));
		gd.cc.DrawRectangle(gd.cc.GetBox(), Color(100,100,100));

		gd.cc.Pop();

		gd.cc.DrawRectangle(gd.cc.GetBox(), Color(0,0,0));

		std::string str;
		str += "SCORE " + pFont->GetNumber(lvl.nScore, 6);
		str += "   ";
		str += "LEVEL " + pFont->GetNumber(lvl.nLevel, 2);
		str += "   ";
		str += "LIVES " + pFont->GetNumber(lvl.nLives, 1);
		str += "   ";
		str += "LENGTH " + pFont->GetNumber(vSnakes[0]->lsPos.size(), 2);
		str += "   ";
		str += "COMBO " + pFont->GetNumber(nCombo, 1);

		gd.cc.Push();

		Size sz = gd.cc.fromR(pFont->szSymbol);

		gd.cc.Translate(fPoint(.5, 0));
		gd.cc.Translate(Point(0, -sz.y/2));

		//pFont->DrawWord(str, gd.cc.toR(Point()), true);
	    
		gd.cc.Pop();
	}

	if(bPreProgrammed && vKeys.size())
	{
		OnKey(vKeys[nKeyPos], false);
		if(++nKeyPos >= vKeys.size())
			nKeyPos = 0;
	}

	if(pDialogBox != 0)
	{
		SP<ActualGame2> pGm(this);
		
		gd.Draw();
		pDialogBox->Update();
		gd.cc.pGr->RefreshAll();
		return;
	}

	//TODO:
	//Move drawing of snake out of "move" function
	//and call it in a more sensible place.  Only then will the below be unnecessary.
	if (ts ==  TS_LOSE)
	{
		for(unsigned i = 0; i < vSnakes.size(); ++i)
		{
			if(vSnakes[i]->bDead)
			{
				vSnakes[i]->stepDeathAnimation();
			}
			vSnakes[i]->Draw();
		}
	}

	if(ts == TS_PLAYING)
	{
		size_t i;
		
		if(!bPreProgrammed)
			plr.SwitchTheme(BG_BACKGROUND);

		for(i = 0; i < vMice.size(); ++i)
		{
			if(vMice[i]->bDead)
				continue;
			//if(pSn->bInvisible)
				vMice[i]->Move();
			//else
			//	vMice[i]->Move(vSnakes[0]->lsPos.front());
		}
		
		//std::vector<SP<Snake2> > vSnakesToMove;
		std::vector<SnakeMoveDescriptor> vSnakeDescr;

		size_t sz = vSnakes.size();
		
		for(i = 0; i < sz; ++i)
		{
			if(vSnakes[i]->tMove.Tick())
			{
				//vSnakesToMove.push_back(vSnakes[i]);
				vSnakeDescr.push_back(SnakeMoveDescriptor(vSnakes[i], true));
			}
			else
				vSnakeDescr.push_back(SnakeMoveDescriptor(vSnakes[i], false));
		}

		for(i = 0; i < sz; ++i)
		{
			if(vSnakeDescr[i].bMoving)
			{
				vSnakeDescr[i].p = vSnakeDescr[i].pSn->GetMove();
				vSnakeDescr[i].ProcessPoint(vSnakes, vSnakeDescr);
			}
		}

		ProcessHeadCollisions(vSnakeDescr);

		for(i = 0; i < sz; ++i)
		{
			if(vSnakeDescr[i].bMoving && gd.mtx.at(vSnakeDescr[i].p).bSolid)
			{
				vSnakeDescr[i].bUnclear = false;
				vSnakeDescr[i].bLose = true;
			}
		}
		
		for(i = 0; i < sz; ++i)
		{
			if(vSnakeDescr[i].bMoving && vSnakeDescr[i].bUnclear)
				vSnakeDescr[i].ProcessTails(vSnakeDescr);
		}

		bool bLose = false;
		for(i = 0; i < sz; ++i)
		{
			if(vSnakeDescr[i].bMoving && !vSnakeDescr[i].bUnclear && vSnakeDescr[i].bLose)
			{
				if(vSnakes[i]->nBoomCounter < 0)
				{
					vSnakes[i]->nBoomCounter = 2;
					vSnakes[i]->tMove.NextTick();
				}
				else if(vSnakes[i]->nBoomCounter > 0)
				{
					vSnakes[i]->nBoomCounter--;
					vSnakes[i]->tMove.NextTick();
				}
				else if(vSnakes[i]->nBoomCounter == 0)
				{
					bLose = true;
					vSnakes[i]->bDead = true;
				}
			}
			else
			{
				vSnakes[i]->nBoomCounter = -1;
			}
		}

		
		if(bLose)
		{
			lvl.nLives = 0;
			ts = TS_LOSE;
			tTerminal = Timer(LOSE_DELAY);
		}
		else
		{
			for(i = 0; i < sz; ++i)
			{
				if(vSnakeDescr[i].bMoving)
					if(!vSnakeDescr[i].bLose)
						vSnakeDescr[i].pSn->DoMove(vSnakeDescr[i].p, vSnakeDescr[i].bTailMovement);
			}
		}
	    
		if(gd.Draw() && !bPreProgrammed)
		{
			for(int i = 0; i < 20; ++i)
			{
				MiceDescr md = lvl.lvCurr.vMice[0];

				Point p;
				p.x = rand()%szTotal.x;
				p.y = rand()%szTotal.y;

				if(!gd.mtx[p].bDraw)
					vMice.push_back(new Mouse(p, md.dRate, gd, pr, md.nRange, md.nWallCode, md.fMaxSpeed, md.fRandomWalk));
			}

			for(int i = 0; i < sz; ++i)
			{
				if(vSnakes[i]->tMove.nPeriod > 2)
					vSnakes[i]->tMove.nPeriod--;
			}
		}

		m_comboClock = std::max(0, m_comboClock - 1);
		if (m_comboClock == 0)
		{
			nCombo = 0;
		}
	}
	else 
	{
		gd.Draw();
		
		if(tTerminal.Tick())
		{
			if(ts == TS_WIN)
			{
				Win();
				return;
			}
			else if (ts == TS_LOSE)
			{
				bool bFinished = true;
				for(unsigned i = 0; i < vSnakes.size(); ++i)
					if(vSnakes[i]->bDead)
						bFinished &= vSnakes[i]->deathAnimationFinished();
				if(bFinished)
				{
					Lose();
					return;
				}
			}
		}
	}
    
    if(!bPreProgrammed)
		gd.cc.pGr->RefreshAll();
}

int ActualGame2::getComboTimeBonus()
{
	return 200;
}

void ActualGame2::Lose(bool bActual/* = true*/)
{
	if(bPreProgrammed)
		return;

	bIgnoreSpaceReturn = true;

	if(!bActual)
	{
		Trigger(pLoseEvent);
		return;
	} 

	pSndMng->PlaySound(pr.GetSnd("win_level"));
	Win();
	return;

	plr.StopMusic();

	if(!bActual)
	{
		Trigger(pLoseEvent);
		return;
	} 
	else
	{	
		pSndMng->PlaySound(pr.GetSnd("gameover"));
	}

	SP<EventAnimationController> pLose = new EventAnimationController(cc, 0, bkgr.rBound, Color(0,0,0),
		pLoseEvent, true);

	SP<AnimationOnce> pLoseAn = new AnimationOnce(pr("over"), cc.fromF(fPoint(.5, .5)), (300/nFrameRate)*3, true,
		NewTerminatorEvent(pLose.GetRawPointer()));

	pLose->NewAnimation(pLoseAn);

	*ppCurrControl = pLose;
}


void ActualGame2::Win()
{
	if(bPreProgrammed)
		return;

	bIgnoreSpaceReturn = true;

	//if(pHigh->nScore < lvl.nScore)
	//	pHigh->nScore = lvl.nScore;
	
	SP<Event> pNext;
	//int nCutSceneNum = lpu.i * 5 + lvl.nLevel;
	
	//int nCutSceneNumFinal = 0;


	//lvl.LoadLevel(rand()%7  + 1);
	//lvl.LoadLevel(int(float(rand())/(float(RAND_MAX)+1)*7)  + 1);

    //lpu.Update(lvl.nLevel);

	pNext = new NewGameEvent2(ppCurrControl, pLoseEvent, cc, szElement,
        bkgr, pr, pFont, pFontCut, lvl, pHigh, lpu, pSel, pSndMng, snDt, ipck);

    //nCutSceneNum = lpu.i * 5 + lvl.nLevel - 1;

	std::string sSnWin = "p1wins";

	if(vSnakes[0]->bDead && vSnakes[1]->bDead)
		sSnWin = "tiegame";
	else if(vSnakes[0]->bDead)
		sSnWin = "p2wins";


	SP<Event> pEv;
	
	if(ipck.gd == GD_PC)
	{
		//SP<CutSceneController2> pCut = new CutSceneController2(pSndMng, pFontCut, cc, pNext, bkgr, 1, pr);
		pEv = pNext;//NewCpSwitchEvent(*ppCurrControl, pCut);
	}
	else
	{
		pEv = pLoseEvent;
	}

	SP<EventAnimationController> pLose = new EventAnimationController(cc, 0, bkgr.rBound, Color(0,0,0),
		pEv, true);

	SP<AnimationTimer> pLoseAn = new AnimationTimer(pr[sSnWin], cc.fromF(fPoint(.5, .5)), (300/nFrameRate)*3, true,
		NewTerminatorEvent(pLose.GetRawPointer()), (300/nFrameRate)*10);

	pLose->NewAnimation(pLoseAn);


	*ppCurrControl = pLose;
}

void ActualGame2::playEatSound()
{
	//int totalMice = lvl.lvCurr.nMice;
	int soundIndex = 12 - gd.nMiceRemaining() - 1;
	playEatSound(soundIndex);
}

void ActualGame2::playEatSound(int index)
{
	//int adjustedIndex = std::max(0, std::min(index, N_EAT_SOUNDS));
	int adjustedIndex = rand()%N_EAT_SOUNDS + 1;

	std::string eatFileStr;
	if (adjustedIndex < 10)
		eatFileStr = "eat_0" + S(adjustedIndex);
	else
		eatFileStr = "eat_" + S(adjustedIndex);

	pSndMng->PlaySound(pr.GetSnd(eatFileStr));
}

void LevelDefinition2::ReadSecondary(MyCamera cc, FilePath& fp, std::string sMiceFile, std::string sPicFile)
{
	if(sPicFile.length() == 0)
		throw SimpleException("LevelDefinition2", "ReadSecondary", "Empty file name!");
	
	if(sPicFile[0] == 'R')
		bRndBoulders = bRndMice = true;
	else if(sPicFile[0] == 'M')
		bRndMice = true;
	else if(sPicFile[0] == 'B')
		bRndBoulders = true;
	
	fp.Parse(sMiceFile);
	fp.Parse(sPicFile);

	std::map<Color, int> mpMiceNumByColor;

	{
		std::map<Color, MiceDescr> mpMiceByColor;

		SP<InStreamHandler> pFl = fp.ReadFile(sMiceFile);
		std::istream& ifs = pFl->GetStream();


		if(ifs.fail())
			throw SimpleException("LevelDefinition2", "ReadSecondary", "Cannot open file " + sMiceFile);

		std::string s;
		std::getline(ifs, s);

		while(true)
		{
			MiceDescr md;
			int nR, nG, nB;

			ifs >> nR >> nG >> nB >> md.nWallCode >> md.nRange >> md.dRate >> md.fMaxSpeed >> md.fRandomWalk;

			Color c(nR, nG, nB);

			if(ifs.fail())
				break;
			mpMiceByColor[c] = md;
		}

		for(std::map<Color, MiceDescr>::iterator itr = mpMiceByColor.begin(), etr = mpMiceByColor.end(); itr != etr; ++itr)
		{
			mpMiceNumByColor[itr->first] = vMice.size();
			vMice.push_back(itr->second);
		}
	}

	{
		Index iImg = cc.pGr->LoadImage(sPicFile);
		Image* pImg = cc.pGr->GetImage(iImg);

		Point p;
		Size sz = pImg->GetSize();

		mtxObjects = Matrix<int>(sz);

		for(p.x = 0; p.x < sz.x; ++p.x)
		for(p.y = 0; p.y < sz.y; ++p.y)
		{
			Color c = pImg->GetPixel(p);
			if(c == Color(120, 120, 120))
				mtxObjects[p] = -1;
			else if(c == Color(255, 255, 255))
			{
				mtxObjects[p] = -2;
				++nRndWall;
			}
			else if(c == Color(168, 230, 29))
			{
				pInPos = p;
				++nDisplayLength;

				for(int nTDr = 0; nTDr < 4; ++nTDr)
				{
					Point pDr = -GetDir(nTDr);
					pDr = p + pDr;
					if(InsideRectangle(Rectangle(sz), pDr))
					{
						if(pImg->GetPixel(pDr) == Color(34,177,76))
							nDir = nTDr;
					}
				}
			}
			else if(c == Color(34,177,76))
			{
				++nDisplayLength;
			}
			else if(c == Color(0, 183, 239))
			{
				pInPos2 = p;
				//++nDisplayLength;

				for(int nTDr = 0; nTDr < 4; ++nTDr)
				{
					Point pDr = -GetDir(nTDr);
					pDr = p + pDr;
					if(InsideRectangle(Rectangle(sz), pDr))
					{
						if(pImg->GetPixel(pDr) == Color(47, 54, 153))
							nDir2 = nTDr;
					}
				}
			}
			else if(c == Color(47, 54, 153))
			{
				//++nDisplayLength;
			}
			else
			{
				std::map<Color, int>::iterator itr = mpMiceNumByColor.find(c);
				if(itr != mpMiceNumByColor.end())
				{
					mtxObjects[p] = itr->second + 1;
					++vMice[itr->second].nRndNum;
				}
				else
					mtxObjects[p] = 0;
			}
				
		}
	}
}

bool LevelInfo2::LoadLevel(unsigned nNewLevel)
{
    if(mpAllLevels.find(nNewLevel) == mpAllLevels.end() || mpAllLevels[nNewLevel].empty())
        return false;

    nLevel = nNewLevel;
    lvCurr = mpAllLevels[nNewLevel][rand()%mpAllLevels[nNewLevel].size()];
    
    return true;
}



void LevelInfo2::Read(MyCamera cc, FilePath& fp, std::string sSuffix)
{
    std::string s = "levelseq" + sSuffix + ".txt";
    fp.Parse(s);

	SP<InStreamHandler> pFl = fp.ReadFile(s);
	std::istream& ifs = pFl->GetStream();
    
    std::getline(ifs, s);

    //int nLength;
	//ifs >> nLength >> nScore >> nLives;
	ifs >> nLives;
	nScore = 0;

	std::getline(ifs, s);
    
    while(true)
    {
		std::getline(ifs, s);
		if(ifs.fail())
			break;

		std::istringstream istr(s);

        int nLevel;
        LevelDefinition2 ld;

		//int nMice;
		//double dMiceRate;
		//int nMiceDistance;
		//int nBoulders;
		//int nWallCode;

        //ifs >> nLevel >> ld.nMice >> ld.dMiceRate >> ld.nMiceDistance >> ld.nBoulders >> ld.nSpeed >> ld.nWallCode;
        //ifs >> nLevel >> nMice >> dMiceRate >> nMiceDistance >> nBoulders >> ld.nSpeed >> nWallCode;
		istr >> nLevel >> ld.nLength >> ld.nSpeed >> ld.bInv;

		std::string sMiceFile;
		std::vector<std::string> vPicFiles;

		istr >> sMiceFile;
		while(true)
		{
			std::string sPicFile;
			istr >> sPicFile;
			if(istr.fail())
				break;
			vPicFiles.push_back(sPicFile);
		}

		for(unsigned j = 0; j < vPicFiles.size(); ++j)
		{
			LevelDefinition2 ldn = ld;
			ldn.ReadSecondary(cc, fp, sMiceFile, vPicFiles[j]);
			mpAllLevels[nLevel].push_back(ldn);
		}

		//ld.bInv = false;
		//ld.nLength = nLength;
		//ld.nDisplayLength = nLength;
		//ld.nDir = 2;
		//ld.pInPos = Point(10,10);

		//ld.mtxObjects = Matrix<int>(Size(10, 10));
		//ld.mtxObjects.at(Point(5,5)) = 1;
		
		//MiceDescr md;
		//md.dRate = .01;
		//md.nRange = 0;
		//md.nWallCode = 0;

		//ld.vMice.push_back(md);

    }
}

CutSceneController2::CutSceneController2(SP< SoundManager > pSndMng_, SP<FontWriter> pFnt_, MyCamera cc_, SP<Event> pTerminate_, Background bkgr_, int nEntry_, MyPreloader& pr_, bool bNoSound_)
    :cc(cc_), pFnt(pFnt_), pTerminate(pTerminate_), bkgr(bkgr_), pr(pr_), nEntry(nEntry_), pSndMng(pSndMng_), bFirst(true), bNoSound(bNoSound_),
	bStart1(false), bStart2(false)
{
    if(nEntry == 0)
        sMessage = "My name is Edora and there are/but three left of our kind";
    else if(nEntry == 1)
        sMessage = "We were once more numerous than/the leaves of an empress tree";
    else if(nEntry == 2)
        sMessage = "Proud and noble species we were";
    else if(nEntry == 3)
        sMessage = "Our land once green and lush...";
    else if(nEntry == 4)
        sMessage = "Whithered now,/this barren cold oasis.";
    else if(nEntry == 5)
        sMessage = "My name is Midora and there are/but two left of our kind";
    else if(nEntry == 6)
        sMessage = "Misguided by our prosperity,/we lost are old ways";
    else if(nEntry == 7)
        sMessage = "Our arrogance blinded us/from our imminent downfall";
    else if(nEntry == 8)
        sMessage = "The prudent few,/we shunned.";
    else if(nEntry == 9)
        sMessage = "All great empires must fall";
    else if(nEntry == 10)
        sMessage = "I am Hidora and I am/the last of my kind";
    else if(nEntry == 11)
        sMessage = "I travel these lands and/ponder the meaning of my existence";
    else if(nEntry == 12)
        sMessage = "Hate is all I feel";
    else if(nEntry == 13)
        sMessage = "Yet I have only myself to blame";
    else if(nEntry == 14)
        sMessage = "I can no longer bear this burden";
    else if(nEntry == 15)
        sMessage = "If only we had acted sooner";
	else
		sMessage = "";
}

void CutSceneController2::OnKey(GuiKeyType nCode, bool bUp)
{
	if(!bUp)
	{
        if(nCode == GUI_ESCAPE)
		{
            Trigger(pTerminate);
			return;
		}

		if(nCode == '1')
			bStart1 = true;
		if(nCode == '2')
			bStart2 = true;

		if(bStart1 && bStart2)
            Trigger(pTerminate);
	}
}

void CutSceneController2::Update()
{
    bkgr.Draw(cc);

	//plr.StopMusic();
	plr.SwitchTheme(BG_BACKGROUND);

	/*
	if(bFirst)
	{
		bFirst = false;
		if(!bNoSound)
		{
			if(nEntry <= 15)
				pSndMng->PlaySound(pr.GetSnd("cutscene_" + S((4 + nEntry-1)%4 + 1)));
			else
			{
				// Alex insert sound here - depending on the final screen (16, 17, or 18)
			}
		}
	}
	*/

    /*
	Size sz = cc.fromR(pFnt->szSymbol);

    std::string s1, s2;

    SeparateMessage(sMessage, s1, s2);
    */
    cc.Push();
    
    cc.Translate(fPoint(.5, .5));
	/*

    int nNum = nEntry%5;
    if(nEntry == 15)
        nNum = 5;
    
    if(nEntry <= 15)
	*/
		//cc.DrawImage(Point(), pr("border").vImage.at(nNum), true);
		cc.DrawImage(Point(), pr["pushstart"], true);
	/*
	else
	{
		if(nEntry == 16)
			cc.DrawImage(Point(), pr["death_1"], true);
		else if(nEntry == 17)
			cc.DrawImage(Point(), pr["death_2"], true);
		else
			cc.DrawImage(Point(), pr["death_3"], true);
	}

    if(s2 != "")
        cc.Translate(Point(0, -cc.fromR(pFnt->GetSize(s1)).y/2));
    
    pFnt->DrawWord(s1, cc.toR(Point()), true, false);

    cc.Translate(Point(0, cc.fromR(pFnt->GetSize(s1)).y));

    pFnt->DrawWord(s2, cc.toR(Point()), true, false);

	*/
    cc.Pop();

    cc.pGr->RefreshAll();
}