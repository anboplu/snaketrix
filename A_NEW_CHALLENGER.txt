::2 player games rules!!::

:basic:
-Snakes may run over invisible snake
-Snakes can bump into anything except mice and invisible snake
-Chooses random level out of a bunch of presets
-

:mechanics:
-snakes start at length 1?
-snakes can control speed
    -other buttons to change speed
    -or double tap in direction to change speed
-speed increases slowly over time
-OR speed depends on length
    -either slower or faster
-1 life
-potentially 3 rounds

:mouse:
-come out of mouse hole
    -global mouse spawn rate
-or random spawning as before

:visual:
-one green snake, blue snake
-display just lives at top left and right in two different color
-VERSUS MODE in the middle
	2            VERSUS MODE            3
-GREEN SNAKE or and BLUE SNAKE LOSES,

